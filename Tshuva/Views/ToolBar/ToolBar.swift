//
//  ToolBar.swift
//  PlayMarket
//
//  Created by Denis W. on 05/08/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit

enum ToolBarButtonType:Int{
    case enterOrLessons = 0, favorites, lessonsDay, share, contact, main
}

@IBDesignable
class ToolBar: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var icons: [UIImageView]!
    @IBOutlet weak var lblEnter: UILabel!
    @IBOutlet weak var lblMain: UILabel!
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(contentView)
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func btnEnterTapped(_ sender: UIButton) {
        sender.monkeyButton()
        
        if User.isRabbi.value {
            if self.parentViewController is RabbiLessonsVC{
                return
            }
            
            Coordinator.shared.pushRabbiLessonsVC()
        }else{
            if self.parentViewController is RabbiEnterVC{
                return
            }
            
            Coordinator.shared.pushRabbiEnterVC()
        }
    }
    
    @IBAction func btnFavoritesTapped(_ sender: UIButton) {
        sender.monkeyButton()
        
        if self.parentViewController is FavoritesVC{
            return
        }
        
        Coordinator.shared.pushFavoritesVC()
        
    }
    
    @IBAction func btnLessonsDayTapped(_ sender: UIButton) {
        sender.monkeyButton()
        
        if self.parentViewController is LessonsDayVC{
            return
        }
        
        Coordinator.shared.pushLessonsDayVC()
        
    }
    
    @IBAction func btnMainTapped(_ sender: UIButton) {
        
        if let mainVC = sender.parentViewController as? MainVC{
            mainVC.viewModel.isMap.accept(!mainVC.viewModel.isMap.value)
            NotificationCenter.default.post(name: UPDATE_TOOLBAR_MAIN_BUTTON, object: nil)
            return
        }
        NAV.popToRootViewController(animated: true)
    }
    
    @IBAction func btnContactTapped(_ sender: UIButton) {
        sender.monkeyButton()
        
        guard let url = URL(string: "mailto:\(MAIL)") else{ return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func btnShareTapped(_ sender: UIButton) {
        sender.monkeyButton()
        
        guard let url = APP_LINK else{ return }
        
        let shareViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        self.parentViewController?.present(shareViewController, animated: true, completion: nil)
    }
    
    
    func updateMainButton(){
        
        if let mainVC = NAV.viewControllers.first(where: { $0 is MainVC }) as? MainVC{
            if self.parentViewController is MainVC{
                icons.first(where: { $0.tag == 5 })?.image = mainVC.viewModel.isMap.value ? UIImage(named: "list_blue") : UIImage(named: "map_blue")
            }else{
                icons.first(where: { $0.tag == 5 })?.image = mainVC.viewModel.isMap.value ? UIImage(named: "list_black") : UIImage(named: "map_black")
            }
            
            lblMain.text = mainVC.viewModel.isMap.value ? "רשימה" : "מפה"
        }
        
    }
    
    func updateEnterButton(){
        
        icons[0].image = UIImage(named: "add_lesson_black")
        lblEnter.text = "שיעורים"
        
    }
    
    func configureButtons(with type:ToolBarButtonType){
        
        icons.forEach { (iv) in
            if iv.tag == type.rawValue{
                switch iv.tag{
                case 0:
                    if self.parentViewController is RabbiEnterVC{
                        iv.image = UIImage(named: "enter_blue")
                        lblEnter.text = "התחברות"
                    }else if self.parentViewController is RabbiLessonsVC{
                        iv.image = UIImage(named: "add_lesson_blue")
                        lblEnter.text = "שיעורים"
                    }
                case 1:
                    iv.image = UIImage(named: "heart_blue")
                case 2:
                    iv.image = UIImage(named: "day_lesson_blue")
                case 3:
                    iv.image = UIImage(named: "share_blue")
                case 4:
                    iv.image = UIImage(named: "contact_blue")
                case 5:
                    if let mainVC = NAV.viewControllers.first(where: { $0 is MainVC }) as? MainVC{
                        iv.image = mainVC.viewModel.isMap.value ? UIImage(named: "list_blue") : UIImage(named: "map_blue")
                        lblMain.text = mainVC.viewModel.isMap.value ? "רשימה" : "מפה"
                    }
                    
                default:
                    break
                }
            }else{
                switch iv.tag{
                case 0:
                    if User.isRabbi.value{
                        iv.image = UIImage(named: "add_lesson_black")
                        lblEnter.text = "שיעורים"
                    }else{
                        iv.image = UIImage(named: "enter_black")
                        lblEnter.text = "התחברות"
                    }
                    
                case 1:
                    iv.image = UIImage(named: "heart_black")
                case 2:
                    iv.image = UIImage(named: "day_lesson_black")
                case 3:
                    iv.image = UIImage(named: "share_black")
                case 4:
                    iv.image = UIImage(named: "contact_black")
                case 5:
                    if let mainVC = NAV.viewControllers.first(where: { $0 is MainVC }) as? MainVC{
                        iv.image = mainVC.viewModel.isMap.value ? UIImage(named: "list_black") : UIImage(named: "map_black")
                        lblMain.text = mainVC.viewModel.isMap.value ? "רשימה" : "מפה"
                    }
                    
                default:
                    break
                }
            }
        }
        
    }
    
}
