//
//  Loader.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class Loader{
    
    private static var progressIndicator:NVActivityIndicatorView?
    private static var progressHoldView:UIView?
    private static var isAlreadyShown:Bool = false
    
    
    class func show(){
        
        if isAlreadyShown{
            return
        }
        
        if progressIndicator == nil{
            progressIndicator = Loader.getProgressIndicatorView(withFrame: CGRect.init(x: (WIDTH/2)-30, y: (HEIGHT/2)-30, width: 60, height: 60), color: .white)
        }
        
        if progressHoldView == nil{
            let bgView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: WIDTH, height: HEIGHT))
            bgView.backgroundColor = .clear
            let centerRectangle = UIView.init(frame: CGRect.init(x: (WIDTH/2)-50, y: (HEIGHT/2)-50, width: 100, height: 100))
            centerRectangle.cornerRadius = 20
            centerRectangle.backgroundColor = Colors.darkBlue //.darkGray
            centerRectangle.alpha = 0.6
            bgView.addSubview(centerRectangle)
            progressHoldView = bgView
        }
        
        DispatchQueue.main.async {
            AppDelegate.getMainDelegate().window?.addSubview(progressHoldView!)
            AppDelegate.getMainDelegate().window?.addSubview(progressIndicator!)
        }
        
        
        isAlreadyShown = true
    }
    
    class func dismiss(){
        DispatchQueue.main.async {
            progressHoldView?.removeFromSuperview()
            progressIndicator?.removeFromSuperview()
        }
        isAlreadyShown = false
    }
    
    class func getProgressIndicatorView(withFrame frame:CGRect, color:UIColor)->NVActivityIndicatorView{
        let progressIndicator = NVActivityIndicatorView.init(frame: frame)
        progressIndicator.color = color
        progressIndicator.type = .lineSpinFadeLoader
        progressIndicator.startAnimating()
        
        return progressIndicator
    }
    
}
