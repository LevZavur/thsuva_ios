//
//  LessonCellViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 20/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RxAnimated
import CoreLocation


class LessonCellViewModel {
    
    var lesson                : Lesson
    let disposeBag            = DisposeBag()
    var favoriteImage         = PublishSubject<UIImage>()
    var favoriteDidTap        = PublishSubject<Void>()
    var openNavigationOptions = PublishSubject<(location: CLLocation, address: String)>()
    var navigateDidTap        = PublishSubject<Void>()
    var rabbiName             = PublishSubject<String>()
    var lessonsData           = PublishSubject<String>()
    var confirmLesson         = PublishSubject<Bool>()
    var confirmLessonDidTap   = PublishSubject<Bool>()
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(lesson: Lesson){
        self.lesson = lesson
        
        favoriteDidTap.subscribe(onNext: { (_) in
            AppData.shared.updateFavorite(with: lesson.rabbiID)
        })
        .disposed(by: disposeBag)
        
        navigateDidTap.subscribe(onNext: { [weak self] (_) in
            guard let location = lesson.address?.location else{
                self?.openNavigationOptions.onError(TshuvaError.locationFailed("Lesson's location error"))
                return
            }
            self?.openNavigationOptions.onNext((location: location, address: lesson.address?.fullAddress ?? ""))
        })
        .disposed(by: disposeBag)
        
        confirmLessonDidTap.subscribe(onNext: { (isConfirmed) in
            AppData.shared.updateConfirmLesson(with: lesson.id, to: isConfirmed)
        })
        .disposed(by: disposeBag)
        
    }
    
    func configurePublishers(){
        
        rabbiName.onNext("- \(lesson.rabbi?.name ?? "") -")
        
        lessonsData.onNext("נושא השיעור: \(lesson.lessonType?.name ?? "")\nשעת השיעור: \(lesson.date.formattedTimeString)\n\(lesson.address?.fullAddress ?? "")\nטלפון: \(lesson.rabbi?.phone ?? "")")
        
        AppData.shared.favoriteRabbiesID
            .map({ [unowned self] in
                $0.contains(self.lesson.rabbiID) ? UIImage(named: "inside info_blue_full")! : UIImage(named: "inside info_blue_big_heart")!
            })
            .bind(to: favoriteImage)
            .disposed(by: disposeBag)
        
        AppData.shared.confirmLessonsID
        .map({ [unowned self] in
            $0.contains(self.lesson.id)
        })
        .bind(to: confirmLesson)
        .disposed(by: disposeBag)
        
    }
    
    
}
