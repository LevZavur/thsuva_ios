//
//  RabbisLessonCell.swift
//  Tshuva
//
//  Created by Denis W. on 24/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class RabbisLessonCell: UITableViewCell {

    @IBOutlet weak var btnEditLesson: UIButton!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    var viewModel: RabbisLessonCellViewModel!{
        didSet{
            self.configureCell()
        }
    }
    
    var disposeBag: DisposeBag! = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    private func configureCell(){
        
        viewModel.lessonType.bind(to: lblSubject.rx.text).disposed(by: disposeBag)
        
        viewModel.date.bind(to: lblDate.rx.text).disposed(by: disposeBag)
        
        viewModel.desc.bind(to: lblDesc.rx.text).disposed(by: disposeBag)
        
        viewModel.address.bind(to: lblAddress.rx.text).disposed(by: disposeBag)
        
        
    }
    

}
