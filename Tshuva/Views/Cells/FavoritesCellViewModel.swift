//
//  FavoritesCellViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 21/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class FavoritesCellViewModel{
    
    var disposeBag              = DisposeBag()
    var favoriteImage           = BehaviorRelay<UIImage?>(value: nil)
    var favoriteDidTap          = PublishSubject<Void>()
    var rabbiName               = BehaviorRelay<String?>(value: nil)
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(rabbi: Rabbi){
        
        favoriteDidTap.subscribe(onNext: { (_) in
            AppData.shared.updateFavorite(with: rabbi.id)
        })
        .disposed(by: disposeBag)
        
        rabbiName.accept("- \(rabbi.name) -")
        
//        AppData.shared.favoriteRabbiesID
//            .map({ $0.contains(rabbi.id) ? UIImage(named: "inside info_blue_full")! : UIImage(named: "inside info_blue_big_heart")! })
//        .bind(to: favoriteImage)
//        .disposed(by: disposeBag)
        
    }
    
}
