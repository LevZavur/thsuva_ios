//
//  FavoritesCell.swift
//  Tshuva
//
//  Created by Denis W. on 21/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAnimated



class FavoritesCell: UITableViewCell {
    
    @IBOutlet weak var lblRabbiName: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    
    var viewModel: FavoritesCellViewModel!{
        didSet{
            self.configureCell()
        }
    }
    
    var disposeBag: DisposeBag! = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    private func configureCell(){
        
        viewModel.rabbiName.bind(to: lblRabbiName.rx.text).disposed(by: disposeBag)
        
//        viewModel.favoriteImage.filter({ [weak self] in
//            $0?.pngData() != self?.btnFavorite.imageView?.image?.pngData()
//
//        }).bind(to: btnFavorite.rx.animated.flip(.right, duration: 0.5).image).disposed(by: disposeBag)
        
        btnFavorite.rx.tap.bind(to: viewModel.favoriteDidTap).disposed(by: disposeBag)
        
    }

}
