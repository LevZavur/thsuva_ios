//
//  LessonCell.swift
//  Tshuva
//
//  Created by Denis W. on 18/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAnimated
import CoreLocation


class LessonCell: UITableViewCell {
    
    @IBOutlet weak var lblRabbiName: UILabel!
    @IBOutlet weak var lblLessonsData: UILabel!
    @IBOutlet weak var switchConfirmation: UISwitch!{
        didSet{
            self.switchConfirmation.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        }
    }
    @IBOutlet weak var btnFavorites: UIButton!
    @IBOutlet weak var btnNavigate: UIButton!
    
    var viewModel: LessonCellViewModel!{
        didSet{
            self.configureCell()
        }
    }
    
    var disposeBag: DisposeBag! = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    private func configureCell(){
        
        viewModel.rabbiName.bind(to: lblRabbiName.rx.text).disposed(by: disposeBag)
        
        viewModel.lessonsData.bind(to: lblLessonsData.rx.text).disposed(by: disposeBag)
        
        viewModel.favoriteImage.filter({ $0.pngData() != self.btnFavorites.imageView?.image?.pngData() }).bind(to: btnFavorites.rx.animated.flip(.right, duration: 0.5).image).disposed(by: disposeBag)
        
        viewModel.confirmLesson.bind(to: switchConfirmation.rx.isOn).disposed(by: disposeBag)
        
        viewModel.openNavigationOptions.subscribe(onNext: { [weak self] (params) in
            self?.parentViewController?.presentNavigateOptions(location: params.location, locationName: params.address)
        }, onError: { (error) in
            SHOW_TOAST(error.localizedDescription)
        })
            .disposed(by: disposeBag)
        
        viewModel.configurePublishers()
        
        btnFavorites.rx.tap.bind(to: viewModel.favoriteDidTap).disposed(by: disposeBag)
        
        switchConfirmation.rx.isOn.bind(to: viewModel.confirmLessonDidTap).disposed(by: disposeBag)
        
        btnNavigate.rx.tap.bind(to: viewModel.navigateDidTap).disposed(by: disposeBag)
        
    }

}
