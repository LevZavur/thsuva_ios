//
//  RabbisLessonViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 24/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class RabbisLessonCellViewModel{
    
    let disposeBag = DisposeBag()
    var lessonType = BehaviorRelay<String?>(value: nil)
    var date       = BehaviorRelay<String?>(value: nil)
    var desc       = BehaviorRelay<String?>(value: nil)
    var address    = BehaviorRelay<String?>(value: nil)
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(lesson: Lesson){
        
        lessonType.accept(lesson.lessonType?.name)
        date.accept("\(lesson.date.formattedHebrewWeekDayString) \(lesson.date.formattedHebrewString) \(lesson.date.formattedTimeString)")
        desc.accept(lesson.info)
        address.accept(lesson.address?.fullAddress)
        
    }
    
}
