//
//  DWPickerView.swift
//  Goodi
//
//  Created by Denis Windover on 27/03/2019.
//  Copyright © 2019 Goodi LTD. All rights reserved.
//

import UIKit

enum DWPickerType{
    case deliveryTime
    case orderBy
    case floor
    case tab
    case deliveryOption
    case string
}

class DWPickerView: UIView {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOk: UIButton!

    
    //MARK: - INIT
    static func getFromNibBy() -> DWPickerView{
        
        let view = UINib(nibName: "DWPickerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DWPickerView
        return view
        
    }
}
