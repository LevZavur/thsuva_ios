//
//  UITextView+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit

extension UITextView{
    func emojiDisabled()->Bool{
        if (self.textInputMode?.primaryLanguage == "emoji") || (self.textInputMode?.primaryLanguage == nil){
            return false
        }
        
        return true
    }
    
    func emojiAndSpaceDisabled(string:String)->Bool{
        if (self.textInputMode?.primaryLanguage == "emoji") || (self.textInputMode?.primaryLanguage == nil){
            return true;
        }
        if self.text == "" && string == " "{
            return true
        }
        
        return false
    }
    
    override open func target(forAction action: Selector, withSender sender: Any?) -> Any? {
        return nil
    }
}
