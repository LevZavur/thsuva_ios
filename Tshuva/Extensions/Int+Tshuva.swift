//
//  Int+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation

extension Int{
    
    var toString: String{
        return "\(self)"
    }
}
