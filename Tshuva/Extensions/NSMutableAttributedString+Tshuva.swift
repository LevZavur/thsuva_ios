//
//  NSMutableAttributedString+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
    
//    @discardableResult func bold(_ text: String, fontSize:CGFloat = 17, color:UIColor = .black, alignment:NSTextAlignment = .right) -> NSMutableAttributedString {
//        let style = NSMutableParagraphStyle()
//        style.alignment = alignment
//        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "VarelaRound-Bold", size: fontSize)!, .foregroundColor:color, .paragraphStyle:style]
//        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
//        append(boldString)
//
//        return self
//    }
    
    @discardableResult func normal(_ text: String, fontSize:CGFloat = 16, color:UIColor = .black, alignment:NSTextAlignment = .right, lineSpacing:CGFloat = 2) -> NSMutableAttributedString {
        let style = NSMutableParagraphStyle()
        style.alignment = alignment
        //style.lineSpacing = lineSpacing
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "FrankRuhlLibre-Medium", size: fontSize)!, .foregroundColor:color, .paragraphStyle:style]
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        append(normal)
        
        return self
    }
    
    @discardableResult func underlined(_ text: String, fontSize:CGFloat = 16, color:UIColor = .black, alignment:NSTextAlignment = .right, lineSpacing:CGFloat = 2) -> NSMutableAttributedString {
        let style = NSMutableParagraphStyle()
        style.alignment = alignment
        //style.lineSpacing = lineSpacing
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont(name: "FrankRuhlLibre-Medium", size: fontSize)!, .foregroundColor:color, .paragraphStyle:style, .underlineStyle: NSUnderlineStyle.single.rawValue]
        let underlined = NSMutableAttributedString(string:text, attributes: attrs)
        append(underlined)
        
        return self
    }
}
