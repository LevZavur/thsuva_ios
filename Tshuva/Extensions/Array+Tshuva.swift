//
//  Array+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation

extension Array{
    
    func rearrange<T>(fromIndex: Int, toIndex: Int) -> Array<T>{
        var arr = self as! Array<T>
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        return arr
    }
    
}
