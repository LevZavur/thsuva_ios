//
//  UITextField+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit

extension UITextField{
    @IBInspectable var placeholderColor:UIColor {
        set {
            if let placeholder = self.placeholder{
                self.attributedPlaceholder = NSAttributedString(string:placeholder, attributes: [NSAttributedString.Key.foregroundColor: newValue])
            }
        }
        get {
            return self.placeholderColor
        }
    }
    
    func emojiDisabled()->Bool{
        if (self.textInputMode?.primaryLanguage == "emoji") || (self.textInputMode?.primaryLanguage == nil){
            return false
        }
        
        return true
    }
    
    func emojiAndSpaceDisabled(string:String)->Bool{
        if (self.textInputMode?.primaryLanguage == "emoji") || (self.textInputMode?.primaryLanguage == nil){
            return false;
        }
        if self.text == "" && string == " "{
            return false
        }
        
        return true
    }
    
    override open func target(forAction action: Selector, withSender sender: Any?) -> Any? {
        return nil
    }
    
}
