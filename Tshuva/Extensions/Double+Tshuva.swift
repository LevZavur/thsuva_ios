//
//  Double+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation

extension Double{
    
    var toString: String{
        return "\(self)"
    }
    
    var clean: String{
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    var rounded2f: String{
        let km = self / 1000
        return String(format: "%.1f", km)
    }
}
