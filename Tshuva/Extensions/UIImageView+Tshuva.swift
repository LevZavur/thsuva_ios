//
//  UIImageView+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import UIKit


extension UIImageView {
    
    @IBInspectable var imageColor:UIColor{
        set{
            self.setImageColor(color: newValue)
        }
        get{
            return self.tintColor
        }
    }
    
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
