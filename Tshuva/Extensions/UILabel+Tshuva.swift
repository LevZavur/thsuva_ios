//
//  UILabel+Tshuva.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit

extension UILabel{
    
    @IBInspectable var isAdjustFontToSizeWidth: Bool{
        set {
            self.adjustsFontSizeToFitWidth = newValue
        }
        get {
            return self.adjustsFontSizeToFitWidth
        }
    }
    
}
