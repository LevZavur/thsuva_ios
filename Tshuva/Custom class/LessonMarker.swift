//
//  LessonMarker.swift
//  Tshuva
//
//  Created by Denis W. on 17/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import GoogleMaps

class LessonMarker: GMSMarker{
    
    var lessons = [Lesson]()
    
    func setIcon(isMultipleLessons: Bool = false){
        self.icon = (isMultipleLessons ? UIImage(named: "green_pin") : UIImage(named: "red_pin"))!.resizeImage(newSize: 30)
    }
    
}
