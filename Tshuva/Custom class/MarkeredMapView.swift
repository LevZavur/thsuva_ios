//
//  MarkeredMapView.swift
//  Tshuva
//
//  Created by Denis W. on 17/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import GoogleMaps

class MarkeredMapView: GMSMapView{
    
    var meMarker: GMSMarker! {
        willSet{
            let index = markers.firstIndex(where: {$0 == self.meMarker})
            if let indexToRemove = index{
                markers.remove(at: indexToRemove)
                self.meMarker.map = nil
            }
            
        }
        didSet{
            addMarker(marker: self.meMarker)
        }
    }
    
    var markers:[GMSMarker] = [] {
        didSet{
            super.clear()
            var newMarkers = markers
            newMarkers.append(meMarker)
            newMarkers.forEach { (maker) in
                maker.map = self
            }
        }
    }
    
    func addMarker(marker: GMSMarker){
        if !markers.contains(marker){
            markers.append(marker)
        }
    }
    
    override public func clear(){
        markers = []
    }

    func clearRetainingMe(){
        clear()
        markers.append(meMarker)
    }
    
}
