//
//  RequestManager.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import RxCocoa
import RxSwift
import GoogleMaps
import GooglePlaces

class RequestManager {
    
    struct APIError{
        enum General: Error{
            case parse, unknown, serverError(String)
        }
        enum CheckAddress: Error{
            case notExists
        }
        enum Login: Error{
            case wrongUsernameOrPassword
        }
        
    }
    
    struct API{
        //Server API
        private static let serverUrl = "https://www.tshuvanow.co.il/api/"
        static let checkAddress = serverUrl + "check-address"
        static let enter = serverUrl + "enter"
        static let login = serverUrl + "login"
        static let createLesson = serverUrl + "create-lesson"
        static let editLesson = serverUrl + "edit-lesson"
        
        
        //Google API
        private static let googleURL = "https://maps.googleapis.com/maps/api/"
        
        static let autocompleteAddress = googleURL + "place/autocomplete/json"
        static let geocodingAddress = googleURL + "geocode/json"
        
    }
    
    static let shared = RequestManager()
    private var manager:SessionManager!
    private var placesClient = GMSPlacesClient()
    
    
    init() {
        manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 60
        //manager.session.configuration.httpShouldSetCookies = true
    }
    
    //MARK: - SERVER REQUESTS
    func editLesson(id: Int, rabbiID: Int, lessonTypeID: Int, date: Double, info: String, placeID: String, address: String, lat: Double, lon: Double) -> Observable<Void>{
        
        Loader.show()
        
        let params = ["id": id,
                      "rabbi_id": rabbiID,
                      "lesson_type_id": lessonTypeID,
                      "timestamp": date,
                      "info": info,
                      "place_id": placeID,
                      "address": address,
                      "lat": lat,
                      "long": lon] as [String : Any]
                
                return Observable.create { observer in
                    self.manager.request(API.editLesson, method: .post, parameters: params)
                        .validate()
                        .responseJSON { response in
                            
                            Loader.dismiss()
                            
                            RequestManager.printRequestDebugDescription(API.editLesson, params: params, response: response)
                            
                            switch response.result{
                            case .success(let data):
                                let checkedResponse = self.checkResponse(data: data)
                                
                                if let error = checkedResponse.error{
                                    observer.onError(error)
                                    return
                                }
                                
                                if let dic = checkedResponse.data{

                                    if let a = dic["address"] as? [String: Any], let address = Mapper<Address>().map(JSON: a){

                                        if !AppData.shared.addresses.contains(where: { $0.placeID == address.placeID }){
                                            AppData.shared.addresses.append(address)
                                        }

                                    }
                                    if let l = dic["lesson"] as? [String: Any], let lesson = Mapper<Lesson>().map(JSON: l){
                                        AppData.shared.lessons.accept(AppData.shared.lessons.value.filter({ $0.id != lesson.id }) + [lesson])
                                    }

                                    observer.onNext(())
                                    return
                                }
                                
                                observer.onError(APIError.General.unknown)
                                
                                
                            case .failure(let error):
                                observer.onError(error)
                            }
                            
                    }
                    return Disposables.create()
                }
        
    }
    func createLesson(rabbiID: Int, lessonTypeID: Int, date: Double, info: String, placeID: String, address: String, lat: Double, lon: Double) -> Observable<Void>{
        
        Loader.show()
        
        let params = ["rabbi_id": rabbiID,
                      "lesson_type_id": lessonTypeID,
                      "timestamp": date,
                      "info": info,
                      "place_id": placeID,
                      "address": address,
                      "lat": lat,
                      "long": lon] as [String : Any]
        
        return Observable.create { observer in
            self.manager.request(API.createLesson, method: .post, parameters: params)
                .validate()
                .responseJSON { response in
                    
                    Loader.dismiss()
                    
                    RequestManager.printRequestDebugDescription(API.createLesson, params: params, response: response)
                    
                    switch response.result{
                    case .success(let data):
                        let checkedResponse = self.checkResponse(data: data)
                        
                        if let error = checkedResponse.error{
                            observer.onError(error)
                            return
                        }
                        
                        if let dic = checkedResponse.data{
                            
                            if let a = dic["address"] as? [String: Any], let address = Mapper<Address>().map(JSON: a){
                                
                                if !AppData.shared.addresses.contains(where: { $0.placeID == address.placeID }){
                                    AppData.shared.addresses.append(address)
                                }
                                
                            }
                            if let l = dic["lesson"] as? [String: Any], let lesson = Mapper<Lesson>().map(JSON: l){
                                AppData.shared.lessons.accept(AppData.shared.lessons.value + [lesson])
                            }
                            
                            observer.onNext(())
                            return
                        }
                        
                        observer.onError(APIError.General.unknown)
                        
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
                    
            }
            return Disposables.create()
        }
        
        
    }
    func login(username: String, password: String) -> Observable<Void>{
        
        Loader.show()
        
        let params = ["uuid": User.uuid,
                      "device_type": User.deviceType,
                      "username": username,
                      "password": password.toMD5] as [String : Any]
        
        return Observable.create { observer in
            self.manager.request(API.login, method: .post, parameters: params)
                .validate()
                .responseJSON { response in
                
                    Loader.dismiss()
                    
                    RequestManager.printRequestDebugDescription(API.login, params: params, response: response)
                    
                    switch response.result{
                    case .success(let data):
                        let checkedResponse = self.checkResponse(data: data)
                        
                        if let error = checkedResponse.error{
                            observer.onError(error)
                            return
                        }
                        
                        if let dic = checkedResponse.data, let rabbi = Mapper<Rabbi>().map(JSON: dic){
                            rabbi.save()
                            observer.onNext(())
                        }else{
                            observer.onError(APIError.General.parse)
                        }
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
                    
                    
            }
            return Disposables.create()
        }
        
    }
    func enter() -> Observable<Void>{
        
        Loader.show()
        
        let params = ["uuid": User.uuid,
                      "device_type": User.deviceType] as [String : Any]
        
        return Observable.create { observer in
            self.manager.request(API.enter, method: .post, parameters: params, encoding: URLEncoding.default)
                .validate()
                .responseJSON { response in
                    
                    Loader.dismiss()
                    
//                    RequestManager.printRequestDebugDescription(API.enter, params: params, response: response)
                    
                    switch response.result{
                    case .success(let data):
                        let checkedResponse = self.checkResponse(data: data)
                        
                        if let error = checkedResponse.error{
                            observer.onError(error)
                            return
                        }
                        
                        if let addresses = checkedResponse.data?["addresses"] as? [[String: Any]]{
                            AppData.shared.addresses = Mapper<Address>().mapArray(JSONArray: addresses)
                        }
                        if let lessonTypes = checkedResponse.data?["lesson_types"] as? [[String: Any]]{
                            AppData.shared.lessonTypes = Mapper<LessonType>().mapArray(JSONArray: lessonTypes)
                        }
                        if let rabbies = checkedResponse.data?["rabbis"] as? [[String: Any]]{
                            AppData.shared.rabbies = Mapper<Rabbi>().mapArray(JSONArray: rabbies)
                        }
                        if let lessons = checkedResponse.data?["lessons"] as? [[String: Any]]{
                            let l = Mapper<Lesson>().mapArray(JSONArray: lessons)
                            AppData.shared.lessons.accept(l)
                        }
                        if let info = checkedResponse.data?["info"] as? [String: Any]{
                            let lessonsDay = Mapper<LessonsDay>().map(JSON: info)
                            AppData.shared.lessonsDay = lessonsDay
                        }
                        
                        observer.onNext(())
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
                    
                    
            }
            return Disposables.create()
        }
    }
    
    func checkIfLocationExistsFor(address: GoogleAddress) -> Observable<GoogleAddress>{
        let params = ["place_id": address.placeID]
        
        return Observable.create { observer in
            self.manager.request(API.checkAddress, method: .post, parameters: params, encoding: URLEncoding.default)
                .validate()
                .responseJSON { response in
                    RequestManager.printRequestDebugDescription(API.checkAddress, params: params, response: response)
                    
                    switch response.result{
                    case .success(let data):
                        let checkedResponse = self.checkResponse(data: data)
                        
                        if let error = checkedResponse.error{
                            observer.onError(error)
                            return
                        }
                        
                        guard let lat = checkedResponse.data?["lat"] as? String, let lon = checkedResponse.data?["long"] as? String else{
                            observer.onError(APIError.CheckAddress.notExists)
                            return
                        }
                        
                        address.lat = lat
                        address.lon = lon
                        observer.onNext(address)
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
    }
    
    //MARK: - GOOGLE API REQUESTS
    func getLocationFor(placeID: String) -> Observable<(lat: Double, lon: Double)>{
        
        let params = ["key": GOOGLE_API_KEY,
                      "place_id": placeID]
        
        Loader.show()
        
        return Observable.create { observer in
            self.manager.request(API.geocodingAddress, method: .get, parameters: params, encoding: URLEncoding.default)
                .validate()
                .responseJSON { response in
                    RequestManager.printRequestDebugDescription(API.geocodingAddress, params: params, response: response)
                    
                    Loader.dismiss()
                    
                    switch response.result{
                    case .success(let data):
                        guard let dic = data as? [String: Any],
                            (dic["status"] as? String) == "OK",
                            let results = dic["results"] as? [[String: Any]] else{
                                observer.onError(response.error ?? APIError.General.parse)
                                return
                        }
                        
                        results.forEach { (e) in
                            if let geometry = e["geometry"] as? [String: Any], let location = geometry["location"] as? [String: Any]{
                                
                                guard let lat = location["lat"] as? Double, let lon = location["lng"] as? Double else{
                                    observer.onError(response.error ?? APIError.General.parse)
                                    return
                                }
                                
                                observer.onNext((lat: lat, lon: lon))
                                return
                            }
                            
                            observer.onError(response.error ?? APIError.General.unknown)
                            
                        }
                        
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
        
        
    }
    
    func fetchAutocompleteAddresses(text: String) -> Observable<[GoogleAddress]>{
        
        let params = ["input": text,
                      "language": "iw",
                      "key": GOOGLE_API_KEY]
        
        return Observable.create { observer in
            self.manager.request(API.autocompleteAddress, method: .get, parameters: params, encoding: URLEncoding.default)
                .validate()
                .responseJSON { response in
                    RequestManager.printRequestDebugDescription(API.autocompleteAddress, params: params, response: response)
                    
                    switch response.result{
                    case .success(let data):
                        guard let dic = data as? [String: Any],
                            (dic["status"] as? String) == "OK",
                            let predictions = dic["predictions"] as? [[String:Any]] else{
                                observer.onError(response.error ?? APIError.General.parse)
                            return
                        }
                        
                        observer.onNext(Mapper<GoogleAddress>().mapArray(JSONArray: predictions))
                        
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            return Disposables.create()
        }
        
    }
    
}

//MARK: - HELPERS
extension RequestManager{
    
    func checkResponse(data: Any) -> (error: Error?, data: [String: Any]?){
        
        if let dic = data as? [String: Any]{
            
            if (dic["error"] as? Int) == 0{
                return (nil, dic["data"] as? [String: Any])
            }
            
            if let errorMessage = dic["error_message"] as? String{
                
                if errorMessage == "wrong username or password"{
                    return (APIError.Login.wrongUsernameOrPassword, nil)
                }
                
                return (APIError.General.serverError(errorMessage), nil)
            }
        }
        
        return (APIError.General.parse, nil)
    }
    
    static fileprivate func json(from object:Any) -> String {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return ""
        }
        return String(data: data, encoding: String.Encoding.utf8) ?? ""
    }
    
    static fileprivate func printRequestDebugDescription(_ request:String?, params:Any? , response:DataResponse<Any>){
        
        switch response.result{
        case .success(let data):
            print("--------------------SERVER REQUEST--------------------")
            print("Request=\(String(describing: request ?? "nil"))\n Params=\(String(describing: params ?? "nil"))\n Response=\(data)")
            print("------------------------------------------------------")
            
        case .failure(let error):
            print("--------------------SERVER REQUEST--------------------")
            print("Request=\(String(describing: request ?? "nil"))\n Params=\(String(describing: params ?? "nil"))\n Error=\(error)")
            print("------------------------------------------------------")
        }
    }
    
    
}
