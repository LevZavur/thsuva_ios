//
//  AppData.swift
//  Tshuva
//
//  Created by Denis W. on 11/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class AppData{
    
    static let shared = AppData()
    
    func start(){
        
        if let fav = UserDefaults.standard.array(forKey: "favoriteRabbiesID") as? [Int]{
            favoriteRabbiesID.accept(fav)
        }
        if let conf = UserDefaults.standard.array(forKey: "confirmLessonsID") as? [Int]{
            confirmLessonsID.accept(conf)
        }
        
    }
    
    
    var addresses         = [Address]()
    var lessonTypes       = [LessonType]()
    var lessonsDay        : LessonsDay?
    var rabbies           = [Rabbi]()
    var lessons           = BehaviorRelay<[Lesson]>(value: [])
    var favoriteRabbiesID = BehaviorRelay<[Int]>(value: [])
    var confirmLessonsID  = BehaviorRelay<[Int]>(value: [])
    
    func updateConfirmLesson(with id: Int, to on: Bool){
        
        if on{
            if !confirmLessonsID.value.contains(id){
                confirmLessonsID.accept(confirmLessonsID.value + [id])
            }
        }else{
            if confirmLessonsID.value.contains(id){
                confirmLessonsID.accept(confirmLessonsID.value.filter({ $0 != id }))
            }
        }
        
        UserDefaults.standard.set(confirmLessonsID.value, forKey: "confirmLessonsID")
        UserDefaults.standard.synchronize()
        
    }
    
    func updateFavorite(with id: Int){
        
        if favoriteRabbiesID.value.contains(id){
            favoriteRabbiesID.accept(favoriteRabbiesID.value.filter({ $0 != id }))
        }else{
            favoriteRabbiesID.accept(favoriteRabbiesID.value + [id])
        }
        
        UserDefaults.standard.set(favoriteRabbiesID.value, forKey: "favoriteRabbiesID")
        UserDefaults.standard.synchronize()
        
    }
    
}
