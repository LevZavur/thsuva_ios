//
//  Coordinator.swift
//  Tshuva
//
//  Created by Denis W. on 14/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import UIKit


class Coordinator{
    
    static let shared = Coordinator()
    
    func pushMainVC(){
        NAV.setAnimationFadeEffect()
        NAV.viewControllers = [MAIN_VC]
    }
    func pushTermsVC(){
        NAV.pushViewController(TERMS_VC, animated: true)
    }
    func pushLessonVC(with lesson: Lesson){
        let vc = LESSON_VC
        let viewModel = LessonViewModel(lesson: lesson)
        vc.viewModel =  viewModel
        NAV.pushViewController(vc, animated: true)
    }
    func pushLessonsVC(with lessons: [Lesson]){
        let vc = LESSONS_VC
        let lessonsViewModel = LessonsViewModel(lessons: lessons)
        vc.viewModel = lessonsViewModel
        NAV.pushViewController(vc, animated: true)
    }
    func pushLessonsDayVC(){
        NAV.pushViewController(LESSONS_DAY_VC, animated: true)
    }
    func pushFavoritesVC(){
        NAV.pushViewController(FAVORITES_VC, animated: true)
    }
    func pushRabbiEnterVC(){
        NAV.pushViewController(RABBI_ENTER_VC, animated: true)
    }
    func pushRabbiLessonsVC(){
        NAV.pushViewController(RABBI_LESSONS_VC, animated: true)
        if NAV.viewControllers.contains(where: { $0 is RabbiEnterVC }){
            NAV.viewControllers = NAV.viewControllers.filter({ !($0 is RabbiEnterVC) })
        }
    }
    
}

extension Coordinator{
    
    private var MAIN_VC: MainVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainVC") as! MainVC
    }
    private var TERMS_VC: TermsVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
    }
    private var LESSON_VC: LessonVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LessonVC") as! LessonVC
    }
    private var LESSONS_VC: LessonsVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LessonsVC") as! LessonsVC
    }
    private var LESSONS_DAY_VC: LessonsDayVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LessonsDayVC") as! LessonsDayVC
    }
    private var FAVORITES_VC: FavoritesVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavoritesVC") as! FavoritesVC
    }
    private var RABBI_ENTER_VC: RabbiEnterVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RabbiEnterVC") as! RabbiEnterVC
    }
    private var RABBI_LESSONS_VC: RabbiLessonsVC{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RabbiLessonsVC") as! RabbiLessonsVC
    }
    
}
