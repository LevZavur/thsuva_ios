//
//  Constants.swift
//  Tshuva
//
//  Created by Denis W. on 07/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import UIKit
import FCUUID



let WIDTH = UIScreen.main.bounds.width

let HEIGHT = UIScreen.main.bounds.height

let WINDOW = UIApplication.shared.keyWindow!

let SYSTEM_VERSION = UIDevice.current.systemVersion

let DEVICE_LANGUAGE = NSLocale.current.localizedString(forLanguageCode: NSLocale.current.languageCode!) ?? ""

let MOBILE_INFO = "[{\"os_version\":\"\(SYSTEM_VERSION)\"},{\"device_config\":\"\(UIDevice.current.model)\"},{\"language\":\"\(DEVICE_LANGUAGE)\"}]"

var MAIL = "tshuvaApp@gmail.com"

var APP_VERSION:String{
    get{
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        //let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        return "v.\(version!)"
    }
}

let UUID = FCUUID.uuidForDevice() ?? ""
let APP_LINK = URL(string: "itms-apps://itunes.apple.com/app/apple-store/id\(APPLE_APP_ID)?mt=8")
let APPLE_APP_ID = "1486801545"
let GOOGLE_API_KEY = "AIzaSyAMb5EX-j6kkCk9pWWPSd3ThCD1_9csPt4"
let ONE_SIGNAL_APP_ID = "fbafdae2-1244-4a93-968d-4abea1e59cc7"

func ERROR_TOAST(){
    SHOW_TOAST("SOMETHING WENT WRONG! TRY AGAIN!")
}

func SERVER_ERROR_TOAST(){
    SHOW_TOAST("שגיאת שרת!")
}

func ERROR_DATA(){
    SHOW_TOAST("DATA CORRUPTED!")
}

func SHOW_TOAST(_ text:String){
    DTIToastCenter.defaultCenter.makeText(text: text)
}
func SHOW_TOAST(text:String, interval:TimeInterval){
    DTIToastCenter.defaultCenter.makeText(text: text, timeInterval:interval)
}

let NAV = UIApplication.shared.keyWindow!.rootViewController as! UINavigationController
