//
//  LocationManager.swift
//  Tshuva
//
//  Created by Denis W. on 12/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import CoreLocation
import RxCocoa
import RxSwift

class LocationManager: NSObject{
    
    
    static let shared = LocationManager()
    static private let defaultLocation = CLLocation.init(latitude: 32.085300, longitude: 34.781769)
    private let manager = CLLocationManager()
    
    override init(){
        super.init()
        manager.delegate = self
        manager.startUpdatingLocation()
    }
    
    var location: BehaviorRelay<CLLocation> = BehaviorRelay(value: LocationManager.defaultLocation)
    
    func start(){
        manager.requestWhenInUseAuthorization()
    }
}

extension LocationManager: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location.accept(locations[0])
    }
    
}
