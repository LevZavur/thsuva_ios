//
//  User.swift
//  Tshuva
//
//  Created by Denis W. on 11/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import ObjectMapper
import RxSwift
import RxCocoa
import OneSignal

class User{
    
    static let uuid = UUID
    static let deviceType = 1
    private static var _rabbi: Rabbi?{
        didSet{
            User.isRabbi.accept(self._rabbi != nil)
        }
    }
    static var rabbi: Rabbi?{
        get{
            if _rabbi == nil{
                if let data = UserDefaults.standard.value(forKey: "rabbi") as? Data{
                    if let rabbi = try? PropertyListDecoder().decode(Rabbi.self, from: data){
                        _rabbi = rabbi
                    }
                }
            }
            return _rabbi
        }
    }
    static var isRabbi = BehaviorRelay<Bool>(value: false)
    static func setRabbi(_ rabbi: Rabbi){
        self._rabbi = rabbi
        OneSignal.sendTag("rabbi", value: "1")
    }
    
    static var isTermsShown: Bool{
        if let _ = UserDefaults.standard.value(forKey: "terms") as? Bool{
            return true
        }
        return false
    }
    static func termsShown(){
        UserDefaults.standard.set(true, forKey: "terms")
        UserDefaults.standard.synchronize()
    }
    
}
