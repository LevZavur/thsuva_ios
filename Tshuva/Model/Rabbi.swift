//
//  Rabbi.swift
//  Tshuva
//
//  Created by Denis W. on 11/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import ObjectMapper

class Rabbi: Mappable, Codable, CustomStringConvertible{
    
    var id    : Int
    var name  : String
    var phone : String
    
    required init?(map: Map) {
        guard let id: Int       = map["id"].value() else{ return nil }
        guard let name: String  = map["name"].value() else{ return nil }
        guard let phone: String = map["phone"].value() else{ return nil }
        
        self.id    = id
        self.name  = name
        self.phone = phone
    }
    
    func mapping(map: Map) {
        
    }
    
    func save(){
        if let encoded = try? PropertyListEncoder().encode(self){
            UserDefaults.standard.set(encoded, forKey: "rabbi")
            User.setRabbi(self)
        }else{
            print("!!!ERROR SAVING RABBI!!!")
        }
    }
    
    var description: String{
        return "id: \(id)\nname: \(name)"
    }
    
}
