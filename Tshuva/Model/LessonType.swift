//
//  LessonType.swift
//  Tshuva
//
//  Created by Denis W. on 11/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import ObjectMapper

class LessonType: Mappable, CustomStringConvertible{
    
    var id  : Int
    var name: String
    
    
    required init?(map: Map) {
        guard let id: Int      = map["id"].value() else{ return nil }
        guard let name: String = map["name"].value() else{ return nil }
        
        self.id   = id
        self.name = name
    }
    
    func mapping(map: Map) {
        
    }
    
    var description: String{
        return "id: \(id)\nname: \(name)"
    }
}



