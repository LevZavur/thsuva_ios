//
//  Address.swift
//  Tshuva
//
//  Created by Denis W. on 10/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreLocation


class Address: Mappable, CustomStringConvertible{
    
    var id         : Int
    var fullAddress: String
    var placeID    : String
    var lat        : String
    var lon        : String
    var location   : CLLocation?{
        guard let _lat = lat.toDouble, let _lon = lon.toDouble else{
            return nil
        }
        return CLLocation(latitude: _lat, longitude: _lon)
    }
    
    required init?(map: Map) {
        guard let id: Int             = map["id"].value() else{ return nil }
        guard let fullAddress: String = map["address"].value() else{ return nil }
        guard let placeID: String     = map["place_id"].value() else{ return nil }
        guard let lat: String         = map["lat"].value() else{ return nil }
        guard let lon: String         = map["long"].value() else{ return nil }
        
        self.id          = id
        self.fullAddress = fullAddress
        self.placeID     = placeID
        self.lat         = lat
        self.lon         = lon
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        fullAddress <- map["address"]
        placeID     <- map["place_id"]
        lat         <- map["lat"]
        lon         <- map["long"]
    }
    
    var description: String {
        return "id: \(id)\nfullAddress: \(fullAddress)\nplaceID: \(placeID)\nlat: \(lat)\nlon: \(lon)"
    }
    
}

class GoogleAddress: Address{
    
    var types: [String] = []
    
    required init?(map: Map) {
        self.types = map["types"].value() ?? []
        
        let latD: Double = map["lat"].value() ?? 0.0
        let lonD: Double = map["lng"].value() ?? 0.0
        
        let json:[String:Any] = ["id":-1, "address": map["description"].value() ?? "",
                                 "place_id": map["place_id"].value() ?? "",
                                 "lat": latD.toString,
                                 "long": lonD.toString]
        let mapping = Map(mappingType: .fromJSON, JSON: json)
        super.init(map: mapping)
    }
    
    override func mapping(map: Map) {

    }
    
}
