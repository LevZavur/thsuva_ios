//
//  Info.swift
//  Tshuva
//
//  Created by Denis W. on 11/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import ObjectMapper

class Info: Mappable, CustomStringConvertible{
    
    var title   : String
    var imageUrl: String
    var body    : String
    
    required init?(map: Map) {
        guard let title   : String = map["title"].value() else{ return nil }
        guard let imageUrl: String = map["imageUrl"].value() else{ return nil }
        guard let body    : String = map["body"].value() else{ return nil }
        
        self.title    = title
        self.imageUrl = imageUrl
        self.body     = body
    }
    
    func mapping(map: Map) {
        
    }
    
    var description: String{
        return "title: \(title)\nimageUrl: \(imageUrl)\nbody: \(body)"
    }
    
}
