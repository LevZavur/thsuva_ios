//
//  Lesson.swift
//  Tshuva
//
//  Created by Denis W. on 11/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import ObjectMapper

class Lesson: Mappable, CustomStringConvertible{
    
    var id          : Int
    var rabbiID     : Int
    var lessonTypeID: Int
    var addressID   : Int
    var info        : String
    var date        : Date
    
    var rabbi: Rabbi?{
        return AppData.shared.rabbies.first(where: { $0.id == rabbiID })
    }
    var lessonType: LessonType?{
        return AppData.shared.lessonTypes.first(where: { $0.id == lessonTypeID })
    }
    var address: Address?{
        return AppData.shared.addresses.first(where: { $0.id == addressID })
    }
    
    required init?(map: Map) {
        guard let id: Int              = map["id"].value() else{ return nil }
        guard let rabbiID: Int         = map["rabbi_id"].value() else{ return nil }
        guard let lessonTypeID: Int    = map["lesson_type_id"].value() else{ return nil }
        guard let addressID: Int       = map["address_id"].value() else{ return nil }
        guard let info: String         = map["info"].value() else{ return nil }
        guard let dateStr: String      = map["timestamp"].value() else{ return nil }
        guard let dateDouble: Double   = dateStr.toDouble else{ return nil }
        
        self.id           = id
        self.rabbiID      = rabbiID
        self.lessonTypeID = lessonTypeID
        self.addressID    = addressID
        self.info         = info
        self.date         = Date(timeIntervalSince1970: dateDouble)
    }
    
    func mapping(map: Map) {
        
    }
    
    var description: String{
        return "id: \(id)\nrabbiID: \(rabbiID)\nlessonType: \(lessonTypeID)\naddressID: \(addressID)\ninfo: \(info)\ndate: \(date)"
    }
    
}
