//
//  LessonsDay.swift
//  Tshuva
//
//  Created by Denis W. on 20/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import ObjectMapper


class LessonsDay: Mappable{
    
    var id   : Int
    var title: String
    var body : String
    var image: String
    
    var imageURL: URL?{
        return URL(string: image)
    }
    
    required init?(map: Map) {
        guard let id: Int       = map["id"].value() else{ return nil }
        guard let title: String = map["title"].value() else{ return nil }
        guard let body: String  = map["body"].value() else{ return nil }
        guard let image: String = map["image"].value() else{ return nil }
        
        self.id    = id
        self.title = title
        self.body  = body
        self.image = image
    }
    
    func mapping(map: Map) {
        
    }
    
}
