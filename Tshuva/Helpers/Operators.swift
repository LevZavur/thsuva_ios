//
//  Operators.swift
//  Tshuva
//
//  Created by Denis W. on 25/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation

// operators for executing a function only if a Bool is true or false

infix operator ==>: NilCoalescingPrecedence
func ==> (lhs: Bool, rhs: ()->Void) {
    if lhs {
        rhs()
    }
}

infix operator !=>: NilCoalescingPrecedence
func !=> (lhs: Bool, rhs: ()->Void) {
    if !lhs {
        rhs()
    }
}

