//
//  Colors.swift
//  Tshuva
//
//  Created by Denis W. on 14/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import UIKit


class Colors{
    
    static let lightBlue = UIColor(r: 41, g: 121, b: 255)
    static let darkBlue = UIColor(r: 0, g: 78, b: 203)
    static let black = UIColor(r: 25, g: 25, b: 25)
    
}
