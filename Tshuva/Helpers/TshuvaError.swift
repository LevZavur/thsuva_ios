//
//  TshuvaError.swift
//  Tshuva
//
//  Created by Denis W. on 20/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation

enum TshuvaError: Error{
    
    case locationFailed(String)
    
    
    var localizedDescription: String{
        switch self {
        case .locationFailed(let errMsg):
            return errMsg
        }
    }
}
