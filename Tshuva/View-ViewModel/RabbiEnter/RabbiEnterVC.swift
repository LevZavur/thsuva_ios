//
//  RabbiEnterVC.swift
//  Tshuva
//
//  Created by Denis W. on 14/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAnimated


class RabbiEnterVC: BaseVC {
    
    
    @IBOutlet weak var lblSubtitle: UILabel!{
        didSet{
            self.lblSubtitle.text = "ברוכים הבאים לתשובה,\nעל מנת להוסיף ולערוך שיעורים\nאנא התחבר."
        }
    }
    @IBOutlet weak var lblDataInvalid: UILabel!
    @IBOutlet weak var viewUsername: UIView!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnEnter: UIButton!
    
    
    var viewModel = RabbiEnterViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        txtUsername.rx.text.orEmpty.bind(to: viewModel.username).disposed(by: disposeBag)
        txtPassword.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: disposeBag)
        btnEnter.rx.tap.bind(to: viewModel.enterDidTap).disposed(by: disposeBag)
        
        viewModel.dataInvalid
            .map({ CGFloat($0 == true ? 1 : 0) })
            .bind(to: lblDataInvalid.rx.animated.fade(duration: 0.5).alpha)
            .disposed(by: disposeBag)
        
        viewModel.dataInvalidText
            .bind(to: lblDataInvalid.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.usernameBorderColor
            .subscribe(onNext: { [weak self] (color) in
                self?.viewUsername.borderColor = color
            })
            .disposed(by: disposeBag)
        
        viewModel.passwordBorderColor
            .subscribe(onNext: { [weak self] (color) in
                self?.viewPassword.borderColor = color
            })
            .disposed(by: disposeBag)
        
    }


}
