//
//  RabbiEnterViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 24/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class RabbiEnterViewModel{
    
    let disposeBag          = DisposeBag()
    var username            = BehaviorRelay<String?>(value: nil)
    var password            = BehaviorRelay<String?>(value: nil)
    var enterDidTap         = PublishSubject<Void>()
    var dataInvalid         = PublishSubject<Bool>()
    var dataInvalidText     = PublishSubject<String>()
    var usernameBorderColor = PublishSubject<UIColor>()
    var passwordBorderColor = PublishSubject<UIColor>()
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    
    init(){
        
        Observable.combineLatest(username, password){ [weak self] _, _ in
            self?.dataInvalid.onNext(false)
            self?.usernameBorderColor.onNext(.white)
            self?.passwordBorderColor.onNext(.white)
        }.subscribe(onNext: { (_) in }).disposed(by: disposeBag)
        
        
        enterDidTap.subscribe(onNext: { [weak self] (_) in
            
            if self?.username.value?.isEmpty == true || self?.password.value?.isEmpty == true{
                if self?.username.value?.isEmpty == true{
                    self?.usernameBorderColor.onNext(.red)
                }
                if self?.password.value?.isEmpty == true{
                    self?.passwordBorderColor.onNext(.red)
                }
                self?.dataInvalidText.onNext("פרטים חסרים")
                self?.dataInvalid.onNext(true)
            }else{
                self?.login()
            }
            
            }).disposed(by: disposeBag)
    }
    
    private func login(){
        
        RequestManager.shared.login(username: username.value ?? "", password: password.value ?? "").subscribe(onNext: { (_) in
            
            Coordinator.shared.pushRabbiLessonsVC()
            
        }, onError: { [weak self] (error) in
            
            switch error{
            case RequestManager.APIError.Login.wrongUsernameOrPassword:
                self?.dataInvalidText.onNext("פרטים שגויים")
                self?.dataInvalid.onNext(true)
                self?.passwordBorderColor.onNext(.red)
                self?.usernameBorderColor.onNext(.red)
                
            default:
                SERVER_ERROR_TOAST()
            }
            
            
        }).disposed(by: disposeBag)
        
    }
    
}
