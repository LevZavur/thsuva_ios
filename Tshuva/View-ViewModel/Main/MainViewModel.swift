//
//  MainViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 12/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import CoreLocation

class MainViewModel{
    
    enum CursorType: String{
        case rabbi = "rabbi", address = "address", type = "type"
    }
    
    let disposeBag = DisposeBag()
    
    //MARK: - INPUT & OUTPUT
    var lessonMarkers             = BehaviorRelay<[LessonMarker]>(value: [])
    var lessonMarkerDidTap        = PublishSubject<LessonMarker>()
    var lessons                   = BehaviorRelay<[Lesson]>(value: [])
    var lessonSelected            = PublishSubject<IndexPath>()
    var navigateToLessons         = PublishSubject<[Lesson]>()
    var isMap                     = BehaviorRelay<Bool>(value: false)
    var currentCursor:CursorType  = .rabbi
    var noResults                 = PublishSubject<Bool>()
    
    var rabbiFilter               = BehaviorRelay<Rabbi?>(value: nil)
    var rabbiSearch               = BehaviorRelay<String>(value: "")
    var rabbiResults              = BehaviorRelay<[Rabbi]>(value: [])
    var rabbiSelected             = PublishSubject<IndexPath>()
    var rabbiViewHeight           = PublishSubject<CGFloat>()
    var rabbiSearchReturnDidTap   = PublishSubject<Void>()
    
    var addressFilter             = BehaviorRelay<Address?>(value: nil)
    var addressSearch             = BehaviorRelay<String>(value: "")
    var addressResults            = BehaviorRelay<[Address]>(value: [])
    var addressSelected           = PublishSubject<IndexPath>()
    var addressViewHeight         = PublishSubject<CGFloat>()
    var addressSearchReturnDidTap = PublishSubject<Void>()
    
    var lessonTypeFilter          = BehaviorRelay<LessonType?>(value: nil)
    var lessonTypeResults         = BehaviorRelay<[LessonType]>(value: [])
    var lessonTypeSelected        = PublishSubject<IndexPath>()
    var lessonTypeViewHeight      = PublishSubject<CGFloat>()
    var lessonTypeOpened          = BehaviorRelay<Bool>(value: false)
    
    //MARK: - INIT
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(){
        configureRabbiSearch()
        configureAddressSearch()
        configureLessonType()
        configureFilterMakers()
        configureLessonObservers()
    }
    
    //MARK: - LESSONS'S OBSERVERS
    fileprivate func configureLessonObservers(){
        
        lessonSelected.map({ [self.lessons.value[$0.row]] }).bind(to: navigateToLessons).disposed(by: disposeBag)
        
        lessonMarkerDidTap.map({ $0.lessons }).bind(to: navigateToLessons).disposed(by: disposeBag)
        
    }
    
    
    //MARK: - FILTER'S OBSERVERS
    fileprivate func configureFilterMakers(){
        
        rabbiFilter.subscribe(onNext: { [weak self] (_) in
            self?.filterMarkers()
        })
        .disposed(by: disposeBag)
        
        addressFilter.subscribe(onNext: { [weak self] (_) in
            self?.filterMarkers()
        })
        .disposed(by: disposeBag)
        
        lessonTypeFilter.subscribe(onNext: { [weak self] (_) in
            self?.filterMarkers()
        })
        .disposed(by: disposeBag)
        
    }
    
    //MARK: - LESSON TYPE FILTER
    fileprivate func configureLessonType(){
        
        lessonTypeOpened.subscribe(onNext: { [weak self] (toOpen) in
            if !toOpen{
                self?.lessonTypeViewHeight.onNext(0)
            }else{
                
                self?.lessonTypeFilter.accept(nil)
                
                let filteredLessonTypes = AppData.shared.lessons.value.filter { (lesson) -> Bool in
                    
                    (self?.rabbiFilter.value != nil ? lesson.rabbiID == self?.rabbiFilter.value?.id : true)
                        &&
                        (self?.addressFilter.value != nil ? lesson.addressID == self?.addressFilter.value?.id : true)
                    
                }.compactMap { (lesson) -> LessonType? in
                    AppData.shared.lessonTypes.first(where: { $0.id == lesson.lessonTypeID })
                }.reduce([]) { (res, lessonType) -> [LessonType] in
                    var r = res
                    if !r.contains(where: { $0.id == lessonType.id }){
                        r.append(lessonType)
                    }
                    return r
                }
                
                if filteredLessonTypes.count == 0{
                    self?.lessonTypeViewHeight.onNext(0)
                    self?.lessonTypeResults.accept([])
                }else{
                    self?.lessonTypeViewHeight.onNext(CGFloat(filteredLessonTypes.count) * 40 + 10)
                    self?.lessonTypeResults.accept(filteredLessonTypes)
                }
            }
        })
        .disposed(by: disposeBag)
        
        lessonTypeSelected
            .map { (indexPath) -> LessonType in
                self.lessonTypeOpened.accept(false)
                return self.lessonTypeResults.value[indexPath.row]
        }
        .bind(to: self.lessonTypeFilter)
        .disposed(by: disposeBag)
        
        
    }
    
    //MARK: - ADDRESS FILTER
    fileprivate func configureAddressSearch(){
        
        addressSearch
            .filter({
                self.rabbiViewHeight.onNext(0)
                return self.addressFilter.value?.fullAddress != $0
            })
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (text) in
                
                if self?.currentCursor == .rabbi { return }
                
                if text.count == 0 && self?.addressFilter.value != nil{
                    self?.addressFilter.accept(nil)
                }
                
                let filteredAddresses = AppData.shared.addresses.filter({ address in address.fullAddress.lowercased().contains(text)
                    &&
                    AppData.shared.lessons.value.contains(where: { $0.addressID == address.id
                        && (self?.rabbiFilter.value != nil ? $0.rabbiID == self?.rabbiFilter.value?.id : true)
                        && (self?.lessonTypeFilter.value != nil ? $0.lessonTypeID == self?.lessonTypeFilter.value?.id : true)
                }) })
                
                if filteredAddresses.count == 0{
                    self?.addressViewHeight.onNext(0)
                    self?.addressResults.accept([])
                }else{
                    if filteredAddresses.count > 5{
                        self?.addressViewHeight.onNext(5 * 40 + 10)
                    }else{
                        self?.addressViewHeight.onNext(CGFloat(filteredAddresses.count) * 40 + 10)
                    }
                    self?.addressResults.accept(filteredAddresses)
                }
            })
            .disposed(by: disposeBag)
        
        addressSearchReturnDidTap.subscribe(onNext: { [weak self] (_) in
            self?.addressFilter.accept(nil)
        })
            .disposed(by: disposeBag)
        
        addressSelected
            .map({ indexPath -> Address in
            self.addressViewHeight.onNext(0)
            return self.addressResults.value[indexPath.row]
        })
            .bind(to: self.addressFilter).disposed(by: disposeBag)
        
    }
    
    //MARK: - RABBI FILTER
    fileprivate func configureRabbiSearch(){
        
        rabbiSearch
            .filter({
                self.addressViewHeight.onNext(0)
                return self.rabbiFilter.value?.name != $0
            })
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (text) in
                
                if self?.currentCursor == .address { return }
                
                if text.count == 0 && self?.rabbiFilter.value != nil{
                    self?.rabbiFilter.accept(nil)
                }
                
                let filteredRabbis = AppData.shared.rabbies.filter({ rabbi in rabbi.name.lowercased().contains(text)
                    &&
                    AppData.shared.lessons.value.contains(where: { $0.rabbiID == rabbi.id
                    && (self?.addressFilter.value != nil ? $0.addressID == self?.addressFilter.value?.id : true)
                    && (self?.lessonTypeFilter.value != nil ? $0.lessonTypeID == self?.lessonTypeFilter.value?.id : true)
                }) })
                
                if filteredRabbis.count == 0{
                    self?.rabbiViewHeight.onNext(0)
                    self?.rabbiResults.accept([])
                }else{
                    if filteredRabbis.count > 5{
                        self?.rabbiViewHeight.onNext(5 * 40 + 10)
                    }else{
                        self?.rabbiViewHeight.onNext(CGFloat(filteredRabbis.count) * 40 + 10)
                    }
                    self?.rabbiResults.accept(filteredRabbis)
                }
            })
            .disposed(by: disposeBag)
        
        rabbiSearchReturnDidTap.subscribe(onNext: { [weak self] (_) in
            self?.rabbiFilter.accept(nil)
        })
            .disposed(by: disposeBag)
        
        rabbiSelected
            .map({ indexPath -> Rabbi in
            self.rabbiViewHeight.onNext(0)
            return self.rabbiResults.value[indexPath.row]
        })
            .bind(to: self.rabbiFilter).disposed(by: disposeBag)
        
    }
    
    //MARK: - FILTER MARKERS
    fileprivate func filterMarkers(){
        
        var dict:[Int: LessonMarker] = [:]
        let lessons = AppData.shared.lessons.value.filter({ $0.date <= Date().addingTimeInterval(24*60*60) }).filter({ (lesson) -> Bool in
            
                (self.rabbiFilter.value != nil ? lesson.rabbiID == self.rabbiFilter.value?.id : true)
                &&
                (self.addressFilter.value != nil ? lesson.addressID == self.addressFilter.value?.id : true)
                &&
                (self.lessonTypeFilter.value != nil ? lesson.lessonTypeID == self.lessonTypeFilter.value?.id : true)
        })
            .sorted(by: { $0.date < $1.date })
        
        
        lessons.forEach({ lesson in
                if let existingMarker = dict[lesson.addressID] {
                    existingMarker.lessons.append(lesson)
                    existingMarker.setIcon(isMultipleLessons: true)
                }else{
                    let marker = LessonMarker()
                    marker.setIcon()
                    marker.position = CLLocationCoordinate2D(latitude: lesson.address?.lat.toDouble ?? 0.0, longitude: lesson.address?.lon.toDouble ?? 0.0)
                    marker.lessons = [lesson]
                    dict[lesson.addressID] = marker
                }
                
            })
        
        let markers = dict.map({ $0.value })
        
        self.noResults.onNext(lessons.count == 0)
        self.lessons.accept(lessons)
        self.lessonMarkers.accept(markers)
        
    }
    
    //MARK: - FETCH ALL DATA
    func fetchData(){
        
        RequestManager.shared.enter()
            .subscribe(onNext: { [weak self] (_) in
                self?.filterMarkers()
            }, onError: { (error) in
                SHOW_TOAST(error.localizedDescription)
            })
            .disposed(by: disposeBag)
        
    }
    
}
