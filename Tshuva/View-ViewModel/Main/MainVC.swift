//
//  MainVC.swift
//  Tshuva
//
//  Created by Denis W. on 12/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import GoogleMaps
import RxCocoa
import RxSwift
import RxGoogleMaps
import RxAnimated



class MainVC: BaseVC{
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var mapView: MarkeredMapView!
    @IBOutlet weak var viewList: UIView!
    @IBOutlet weak var tblLessonList: UITableView!
    
    
    @IBOutlet weak var txtRabbiSearch: UITextField!
    @IBOutlet weak var tblRabbiSearch: UITableView!
    @IBOutlet weak var constrHeightViewRabbiSearch: NSLayoutConstraint!
    
    @IBOutlet weak var txtAddressSearch: UITextField!
    @IBOutlet weak var tblAddressSearch: UITableView!
    @IBOutlet weak var constrHeightViewAddressSearch: NSLayoutConstraint!
    
    @IBOutlet weak var ivArrow: UIImageView!
    @IBOutlet weak var lblLessonType: UILabel!
    @IBOutlet weak var btnLessonType: UIButton!
    @IBOutlet weak var tblLessonType: UITableView!
    @IBOutlet weak var constrHeightViewLessonType: NSLayoutConstraint!
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var viewNoResults: UIView!
    @IBOutlet weak var btnSendFeedback: UIButton!
    
    
    let viewModel = MainViewModel()
    let MAX_ZOOM:Float = 18
    let MIN_ZOOM:Float = 11
    let DEFAULT_ZOOM:Float = 15

    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMap()
        rabbiSearchBindings()
        addressSearchBindings()
        lessonTypeBindings()
        lessonsBindings()
        
        btnSendFeedback.rx.tap.subscribe(onNext: { (_) in
            guard let url = URL(string: "mailto:\(MAIL)") else{ return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        })
            .disposed(by: disposeBag)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.fetchData()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewShadow.dropShadow(onlyBottom: true, color: .black)
    }

    
    //MARK: - HELPERS
    fileprivate func initMap(){
        
        mapView.setMinZoom(MIN_ZOOM, maxZoom: MAX_ZOOM)
        
        LocationManager.shared.location.asObservable()
            .take(2)
            .map({ GMSCameraPosition(latitude: $0.coordinate.latitude, longitude: $0.coordinate.longitude, zoom: self.DEFAULT_ZOOM) })
            .bind(to: mapView.rx.cameraToAnimate)
            .disposed(by: disposeBag)
        
         LocationManager.shared.location.asObservable()
            .take(2)
            .subscribe( onNext: { location in
                let meMarker = GMSMarker(position: location.coordinate)
                meMarker.icon = UIImage(named: "map_my_location")?.resizeImage(newSize: 30)
                self.mapView.meMarker = meMarker
               })
            .disposed(by: disposeBag)
        
        viewModel.isMap
            .map({ $0 == true ? 0 : 1 })
            .bind(to: viewList.rx.animated.fade(duration: 0.5).alpha)
            .disposed(by: disposeBag)
        
        viewModel.lessonMarkers
            .subscribe(onNext: { [weak self] (markers) in
                self?.mapView.markers = markers
            })
            .disposed(by: disposeBag)
        
    }
    
    fileprivate func rotateArrow(open: Bool){
        let position = open ? CGAffineTransform(rotationAngle: CGFloat(Double.pi * 1)) : CGAffineTransform.identity
        
        UIView.animate(withDuration: 0.25, animations: {
            if self.ivArrow.transform != position{
                self.ivArrow.transform = position
            }
        }) { (completed) in}
    }
    
    //MARK: - BINDINGS
    fileprivate func lessonsBindings(){
        
        tblLessonList.rx.itemSelected.bind(to: viewModel.lessonSelected).disposed(by: disposeBag)
        
        
        mapView.rx.handleTapMarker { [weak self] marker in
            if let m = marker as? LessonMarker{
                self?.viewModel.lessonMarkerDidTap.onNext(m)
            }
            return false
        }
        
        viewModel.lessons
            .bind(to: tblLessonList.rx.items(cellIdentifier: "LessonCell", cellType: LessonCell.self) ){ row, lesson, cell in
                cell.viewModel = LessonCellViewModel(lesson: lesson)
        }
        .disposed(by: disposeBag)
        
        
        viewModel.navigateToLessons.subscribe(onNext: { (lessons) in
            if lessons.count == 1{
                Coordinator.shared.pushLessonVC(with: lessons[0])
            }else{
                Coordinator.shared.pushLessonsVC(with: lessons)
            }
        })
            .disposed(by: disposeBag)
        
        viewModel.noResults.map({ $0 == true ? 1 : 0 }).bind(animated: viewNoResults.rx.animated.fade(duration: 0.5).alpha).disposed(by: disposeBag)
        
    }
    fileprivate func lessonTypeBindings(){
        
        btnLessonType.rx.tap.map({ _ in
            if self.viewModel.rabbiFilter.value?.name != self.txtRabbiSearch.text{
                self.txtRabbiSearch.text = ""
            }
            if self.viewModel.addressFilter.value?.fullAddress != self.txtAddressSearch.text{
                self.txtAddressSearch.text = ""
            }
            self.view.endEditing(true)
            self.viewModel.currentCursor = .type
            return !self.viewModel.lessonTypeOpened.value
        })
            .bind(to: viewModel.lessonTypeOpened).disposed(by: disposeBag)
        
        tblLessonType.rx.itemSelected.bind(to: viewModel.lessonTypeSelected).disposed(by: disposeBag)
        
        viewModel.lessonTypeResults
            .bind(to: tblLessonType.rx.items){ tableView, row, lessonType in
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                cell.textLabel?.text = lessonType.name
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .white
                cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                return cell
        }
        .disposed(by: disposeBag)
        
        viewModel.lessonTypeViewHeight
            .subscribe(onNext: { [weak self] (height) in
                UIView.animate(withDuration: 0.5) {
                    self?.constrHeightViewLessonType.constant = height
                    self?.view.layoutIfNeeded()
                }
        })
            .disposed(by: disposeBag)
        
        viewModel.lessonTypeFilter
        .subscribe(onNext: { [weak self] (lessonType) in
            self?.lblLessonType.text = lessonType != nil ? lessonType!.name : "חפש נושא שיעור"
        })
        .disposed(by: disposeBag)
        
        viewModel.lessonTypeOpened.subscribe(onNext: { [weak self] (open) in
            self?.rotateArrow(open: open)
        })
            .disposed(by: disposeBag)
        
    }
    
    fileprivate func addressSearchBindings(){
        
        txtAddressSearch.rx.text.orEmpty.map({ [weak self] in
            if self?.viewModel.rabbiFilter.value?.name != self?.txtRabbiSearch.text{
                self?.txtRabbiSearch.text = ""
            }
            self?.viewModel.currentCursor = .address
            self?.viewModel.lessonTypeOpened.accept(false)
            return $0
        }).bind(to: viewModel.addressSearch).disposed(by: disposeBag)
        
        txtAddressSearch.rx.controlEvent([.editingDidEndOnExit]).map({
            self.txtAddressSearch.text = ""
            self.txtAddressSearch.sendActions(for: .valueChanged)
            return $0
        })
            .bind(to: viewModel.addressSearchReturnDidTap)
            .disposed(by: disposeBag)
        
        tblAddressSearch.rx.itemSelected.bind(to: viewModel.addressSelected).disposed(by: disposeBag)
        
        viewModel.addressFilter
            .map({ $0?.fullAddress })
            .subscribe(onNext: { [weak self] (fullAddress) in
                self?.txtAddressSearch.text = fullAddress
                if fullAddress != nil{
                    self?.view.endEditing(true)
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.addressResults
            .bind(to: tblAddressSearch.rx.items){ tableView, row, address in
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                cell.textLabel?.text = address.fullAddress
                cell.textLabel?.textAlignment = .right
                cell.textLabel?.textColor = .white
                cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                return cell
        }
        .disposed(by: disposeBag)
        
        viewModel.addressViewHeight
            .subscribe(onNext: { [weak self] (height) in
                UIView.animate(withDuration: 0.5) {
                    self?.constrHeightViewAddressSearch.constant = height
                    self?.view.layoutIfNeeded()
                }
        })
            .disposed(by: disposeBag)
        
    }
    
    fileprivate func rabbiSearchBindings(){
        
        txtRabbiSearch.rx.text.orEmpty.map({ [weak self] in
            if self?.viewModel.addressFilter.value?.fullAddress != self?.txtAddressSearch.text{
                self?.txtAddressSearch.text = ""
            }
            self?.viewModel.currentCursor = .rabbi
            self?.viewModel.lessonTypeOpened.accept(false)
            return $0
        })
            .bind(to: viewModel.rabbiSearch).disposed(by: disposeBag)
        
        txtRabbiSearch.rx.controlEvent([.editingDidEndOnExit]).map({
            self.txtRabbiSearch.text = ""
            self.txtRabbiSearch.sendActions(for: .valueChanged)
            return $0
        })
            .bind(to: viewModel.rabbiSearchReturnDidTap)
            .disposed(by: disposeBag)
        
        tblRabbiSearch.rx.itemSelected.bind(to: viewModel.rabbiSelected).disposed(by: disposeBag)
        
        viewModel.rabbiFilter
            .map({ $0?.name })
            .subscribe(onNext: { [weak self] (rabbiName) in
                self?.txtRabbiSearch.text = rabbiName
                if rabbiName != nil{
                    self?.view.endEditing(true)
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.rabbiResults
            .bind(to: tblRabbiSearch.rx.items){ tableView, row, rabbi in
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                cell.textLabel?.text = rabbi.name
                cell.textLabel?.textAlignment = .right
                cell.textLabel?.textColor = .white
                cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                return cell
        }
        .disposed(by: disposeBag)
        
        viewModel.rabbiViewHeight
            .subscribe(onNext: { [weak self] (height) in
                UIView.animate(withDuration: 0.5) {
                    self?.constrHeightViewRabbiSearch.constant = height
                    self?.view.layoutIfNeeded()
                }
        })
            .disposed(by: disposeBag)
        
    }

}
