//
//  SplashVC.swift
//  Tshuva
//
//  Created by Denis W. on 14/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SplashVC: UIViewController {
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var btnBigApps: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBigApps.rx.tap.subscribe(onNext: { (_) in
            
            guard let url = URL(string: "http://bigapps.co.il") else{ return }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        }).disposed(by: disposeBag)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if User.isTermsShown{
                Coordinator.shared.pushMainVC()
            }else{
                Coordinator.shared.pushTermsVC()
            }
        }
    }

}
