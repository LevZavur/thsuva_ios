//
//  BaseVC.swift
//  Tshuva
//
//  Created by Denis W. on 13/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

let UPDATE_TOOLBAR_MAIN_BUTTON = NSNotification.Name.init("updateToolbarMainButton")

class BaseVC: UIViewController {
    
    @IBOutlet weak var toolBar: ToolBar! = nil
    
    let disposeBag = DisposeBag()
    

    //MARK: - LIFECYCLE
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self is MainVC || self is FavoritesVC || self is RabbiLessonsVC || self is RabbiEnterVC || self is LessonsDayVC{
            
            NotificationCenter.default.addObserver(self, selector: #selector(updateButtons), name: UPDATE_TOOLBAR_MAIN_BUTTON, object: nil)
            
            User.isRabbi
                .filter({ $0 == true })
                .subscribe(onNext: { [weak self] (_) in
                    self?.toolBar?.updateEnterButton()
                })
                .disposed(by: disposeBag)
            
            configureToolBar()
        }
    }
    
    @objc func updateButtons(){
        toolBar?.updateMainButton()
    }
    
    //MARK: - PRIVATE METHODS
    private func configureToolBar(){
        
        switch self {
        case is MainVC:
            toolBar?.configureButtons(with: .main)
        case is FavoritesVC:
            toolBar?.configureButtons(with: .favorites)
        case is LessonsDayVC:
            toolBar?.configureButtons(with: .lessonsDay)
        case is RabbiEnterVC, is RabbiLessonsVC:
            toolBar.configureButtons(with: .enterOrLessons)
        
        default:
            break
        }
        
        
    }
    
    //MARK: - BACK BUTTON
    @IBAction func btnBackTapped(_ sender: UIButton) {
        sender.monkeyButton()
        
        if let vc = self as? RabbiLessonsVC{
            if vc.viewModel.isAddLessonMode.value{
                vc.viewModel.isAddLessonMode.accept(false)
                return
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
