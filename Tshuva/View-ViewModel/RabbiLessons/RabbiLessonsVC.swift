//
//  RabbiLessonsVC.swift
//  Tshuva
//
//  Created by Denis W. on 14/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAnimated


class RabbiLessonsVC: BaseVC {
    
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var btnAddLesson: UIButton!
    @IBOutlet weak var viewNoResults: UIView!
    @IBOutlet weak var tblLessons: UITableView!
    
    @IBOutlet weak var viewAddLesson: UIView!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var tblAddress: UITableView!
    @IBOutlet weak var constrHeightViewAddress: NSLayoutConstraint!
    
    @IBOutlet weak var ivArrow: UIImageView!
    @IBOutlet weak var lblLessonType: UILabel!
    @IBOutlet weak var btnLessonType: UIButton!
    @IBOutlet weak var tblLessonType: UITableView!
    @IBOutlet weak var constrHeightViewLessonType: NSLayoutConstraint!
    
    @IBOutlet weak var textVDesc: UITextView!
    @IBOutlet weak var lblDescCount: UILabel!
    
    
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var btnYear: UIButton!
    @IBOutlet weak var tblYear: UITableView!
    @IBOutlet weak var constrHeightViewYear: NSLayoutConstraint!
    
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var tblMonth: UITableView!
    @IBOutlet weak var constrHeightViewMonth: NSLayoutConstraint!
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var btnDay: UIButton!
    @IBOutlet weak var tblDay: UITableView!
    @IBOutlet weak var constrHeightViewDay: NSLayoutConstraint!
    

    @IBOutlet weak var txtHour: UITextField!
    @IBOutlet weak var txtMinute: UITextField!
    
    
    @IBOutlet weak var btnCreateLesson: UIButton!
    
    
    
    
    var viewModel = RabbiLessonsViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnAddLesson.rx.tap.map({ true }).bind(to: viewModel.isAddLessonMode).disposed(by: disposeBag)
        btnCreateLesson.rx.tap.bind(to: viewModel.createLesson).disposed(by: disposeBag)
        
        datePickerBindings()
        lessonsBindings()
        modeBindings()
        descriptionBindings()
        addressBindings()
        yearBindings()
        monthBindings()
        dayBindings()
        lessonTypeBindings()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewShadow.dropShadow(onlyBottom: true, color: .black)
    }
    
    private func lessonsBindings(){
        
        tblLessons.rx.itemSelected.bind(to: viewModel.lessonDidTap).disposed(by: disposeBag)
        
        viewModel.lessons.bind(to: tblLessons.rx.items(cellIdentifier: "RabbisLessonCell", cellType: RabbisLessonCell.self)){ [weak self] row, lesson, cell in
            guard let `self` = self else{ return }
            cell.viewModel = RabbisLessonCellViewModel(lesson: lesson)
            
            cell.btnEditLesson.rx.tap.subscribe(onNext: { [weak self] (_) in
                self?.txtAddress.text = lesson.address?.fullAddress
                self?.lblLessonType.text = lesson.lessonType?.name
                self?.textVDesc.text = lesson.info
                self?.lblYear.text = lesson.date.formattedHebrewYearString
                self?.lblMonth.text = lesson.date.formattedHebrewMonthString
                self?.lblDay.text = lesson.date.formattedHebrewDayString
                
            }).disposed(by: cell.disposeBag)
            
            cell.btnEditLesson.rx.tap.map({ lesson }).bind(to: self.viewModel.editLesson).disposed(by: cell.disposeBag)
            
        }.disposed(by: disposeBag)
        
    }
    
    //MARK: - MODE BINDINGS
    private func modeBindings(){
        
        viewModel.noLessons
            .map({ $0 == true ? 1 : 0 })
            .bind(to: viewNoResults.rx.animated.fade(duration: 0.5).alpha)
            .disposed(by: disposeBag)
        
        viewModel.isAddLessonMode
            .map({ [weak self] in
                if !$0{
                    self?.txtAddress.text = nil
                    self?.lblLessonType.text = "בחר נושא"
                    self?.textVDesc.text = nil
                    self?.lblDescCount.text = "0/140"
                    self?.lblDay.text = "בחר יום"
                    self?.lblMonth.text = "בחר חודש"
                    self?.lblYear.text = "בחר שנה"
                    self?.txtHour.text = nil
                    self?.txtMinute.text = nil
                }
                return $0 == true ? 1 : 0
                
            })
            .bind(to: viewAddLesson.rx.animated.fade(duration: 0.5).alpha)
            .disposed(by: disposeBag)
        
    }
    
    //MARK: - LESSON'S TYPE BINDINGS
    private func lessonTypeBindings(){
        
        btnLessonType.rx.tap.map({ [weak self] in
            
            if self?.viewModel.address.value?.fullAddress != self?.txtAddress.text{
                self?.txtAddress.text = ""
            }
            self?.view.endEditing(true)
            self?.viewModel.currentCursor = .lessonType
            return !(self?.viewModel.lessonTypeOpened.value ?? false)
            }).bind(to: viewModel.lessonTypeOpened).disposed(by: disposeBag)
        
        tblLessonType.rx.itemSelected.bind(to: viewModel.lessonTypeSelected).disposed(by: disposeBag)
        
        viewModel.lessonTypeResults
            .bind(to: tblLessonType.rx.items){ tableView, row, lessonType in
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                cell.textLabel?.text = lessonType.name
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .white
                cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                return cell
        }
        .disposed(by: disposeBag)
        
        viewModel.lessonTypeViewHeight.bind(to: constrHeightViewLessonType.rx.animated.layout(duration: 0.5).constant ).disposed(by: disposeBag)
        
        viewModel.lessonType.map({ $0?.name ?? "בחר נושא" }).bind(to: lblLessonType.rx.text)
        .disposed(by: disposeBag)
        
        viewModel.lessonTypeOpened.subscribe(onNext: { [weak self] (open) in
            self?.rotateArrow(open: open)
        })
            .disposed(by: disposeBag)
        
    }
    
    //MARK: - DAY BINDINGS
    private func dayBindings(){
        
        btnDay.rx.tap.map({ [weak self] in
            if self?.viewModel.address.value?.fullAddress != self?.txtAddress.text{
                self?.txtAddress.text = ""
            }
            self?.view.endEditing(true)
            self?.viewModel.currentCursor = .day
            return !(self?.viewModel.dayOpened.value ?? false)
            }).bind(to: viewModel.dayOpened).disposed(by: disposeBag)
        
        tblDay.rx.itemSelected.bind(to: viewModel.dayDidSelect).disposed(by: disposeBag)
        
        viewModel.days
            .bind(to: tblDay.rx.items){ tableView, row, day in
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                cell.textLabel?.text = day
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .white
                cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                return cell
        }
        .disposed(by: disposeBag)
        
        viewModel.dayViewHeight.bind(to: constrHeightViewDay.rx.animated.layout(duration: 0.5).constant ).disposed(by: disposeBag)
        
        viewModel.day.map({ $0 ?? "בחר יום" }).bind(to: lblDay.rx.text)
        .disposed(by: disposeBag)
        
    }
    
    //MARK: - MONTH BINDINGS
    private func monthBindings(){
        
        btnMonth.rx.tap.map({ [weak self] in
             if self?.viewModel.address.value?.fullAddress != self?.txtAddress.text{
                 self?.txtAddress.text = ""
             }
             self?.view.endEditing(true)
             self?.viewModel.currentCursor = .month
             return !(self?.viewModel.monthOpened.value ?? false)
             }).bind(to: viewModel.monthOpened).disposed(by: disposeBag)
         
         tblMonth.rx.itemSelected.bind(to: viewModel.monthDidSelect).disposed(by: disposeBag)
         
         viewModel.months
             .bind(to: tblMonth.rx.items){ tableView, row, month in
                 let cell = UITableViewCell()
                 cell.selectionStyle = .none
                 cell.backgroundColor = .clear
                 cell.textLabel?.text = month
                 cell.textLabel?.textAlignment = .center
                 cell.textLabel?.textColor = .white
                 cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                 return cell
         }
         .disposed(by: disposeBag)
         
         viewModel.monthViewHeight.bind(to: constrHeightViewMonth.rx.animated.layout(duration: 0.5).constant ).disposed(by: disposeBag)
         
         viewModel.month.map({ $0 ?? "בחר חודש" }).bind(to: lblMonth.rx.text)
         .disposed(by: disposeBag)
        
    }
    
    //MARK: - YEAR BINDINGS
    private func yearBindings(){
        
        btnYear.rx.tap.map({ [weak self] in
            if self?.viewModel.address.value?.fullAddress != self?.txtAddress.text{
                self?.txtAddress.text = ""
            }
            self?.view.endEditing(true)
            self?.viewModel.currentCursor = .year
            return !(self?.viewModel.yearOpened.value ?? false)
            }).bind(to: viewModel.yearOpened).disposed(by: disposeBag)
        
        tblYear.rx.itemSelected.bind(to: viewModel.yearDidSelect).disposed(by: disposeBag)
        
        viewModel.years
            .bind(to: tblYear.rx.items){ tableView, row, year in
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                cell.textLabel?.text = year
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = .white
                cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                return cell
        }
        .disposed(by: disposeBag)
        
        viewModel.yearViewHeight.bind(to: constrHeightViewYear.rx.animated.layout(duration: 0.5).constant ).disposed(by: disposeBag)
        
        viewModel.year.map({ $0 ?? "בחר שנה" }).bind(to: lblYear.rx.text)
        .disposed(by: disposeBag)
        
    }
    
    //MARK: - ADDRESS BINDINGS
    private func addressBindings(){
        
        txtAddress.rx.text.orEmpty.map { [weak self] in
            self?.viewModel.currentCursor = .address
            return $0
            }.bind(to: viewModel.addressSearch).disposed(by: disposeBag)
        
        txtAddress.rx.controlEvent([.editingDidEndOnExit]).map ({ [weak self] in
            self?.txtAddress.text = ""
            self?.txtAddress.sendActions(for: .valueChanged)
            return $0
            }).bind(to: viewModel.addressSearchReturnDidTap)
            .disposed(by: disposeBag)
        
        tblAddress.rx.itemSelected.bind(to: viewModel.addressSelected).disposed(by: disposeBag)
        
        viewModel.address.map({ $0?.fullAddress }).subscribe(onNext: { [weak self] (fullAddress) in
            self?.txtAddress.text = fullAddress
            self?.view.endEditing(true)
            }).disposed(by: disposeBag)
        
        viewModel.addressResults
            .bind(to: tblAddress.rx.items){ tableView, row, address in
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                cell.backgroundColor = .clear
                cell.textLabel?.text = address.fullAddress
                cell.textLabel?.textAlignment = .right
                cell.textLabel?.textColor = .white
                cell.textLabel?.font = UIFont(name: "FrankRuhlLibre-Regular", size: 17)!
                return cell
        }.disposed(by: disposeBag)
        
        viewModel.addressViewHeight.bind(to: constrHeightViewAddress.rx.animated.layout(duration: 0.5).constant ).disposed(by: disposeBag)
        
    }
    
    //MARK: - DESCRIPTION BINDINGS
    private func descriptionBindings(){
        
        textVDesc.rx.text.orEmpty.scan("") { (prev, new) -> String in
            
            if new.count > 140 || new.containsEmoji{
                return prev ?? String(new.prefix(140))
            }else{
                return new
            }
            }.subscribe(textVDesc.rx.text).disposed(by: disposeBag)
        
        textVDesc.rx.text.orEmpty
            .bind(to: viewModel.desc)
            .disposed(by: disposeBag)
        
        textVDesc.rx.text.orEmpty
            .filter({ String($0.last ?? Character("c")) == "\n" })
            .subscribe(onNext: { [weak self] (_) in
                self?.view.endEditing(true)
            }).disposed(by: disposeBag)
        
        textVDesc.rx.didBeginEditing.map({ [weak self] in
            if self?.viewModel.address.value?.fullAddress != self?.txtAddress.text{
                self?.txtAddress.text = ""
                self?.viewModel.currentCursor = .desc
            }
        }).bind(to: viewModel.descDidBeginEditing).disposed(by: disposeBag)
        
        viewModel.descCount.bind(to: lblDescCount.rx.text).disposed(by: disposeBag)
        
    }
    
    //MARK: - DATE PICKER BINDINGS
    private func datePickerBindings(){
        
        let datePicker = DWPickerView.getFromNibBy()
        txtHour.inputView = datePicker
        txtMinute.inputView = datePicker
        
        datePicker.btnCancel.rx.tap.subscribe(onNext: { [weak self] (_) in
            self?.view.endEditing(true)
        }).disposed(by: disposeBag)
        
        datePicker.btnOk.rx.tap.map({ (hours: datePicker.datePicker.date.hours, minutes: datePicker.datePicker.date.minutes) }).bind(to: viewModel.hoursAndMinutes).disposed(by: disposeBag)
        
        viewModel.hoursAndMinutes.filter({ $0 != nil }).subscribe(onNext: { [weak self] (values) in
            self?.txtHour.text = values?.hours.toString
            self?.txtMinute.text = values?.minutes.toString == "0" ? "00" : values?.minutes.toString
            self?.view.endEditing(true)
        }).disposed(by: disposeBag)
        
    }
    
    //MARK: - HELPERS
    fileprivate func rotateArrow(open: Bool){
        let position = open ? CGAffineTransform(rotationAngle: CGFloat(Double.pi * 1)) : CGAffineTransform.identity
        
        UIView.animate(withDuration: 0.25, animations: {
            if self.ivArrow.transform != position{
                self.ivArrow.transform = position
            }
        }) { (completed) in}
    }
    

}


