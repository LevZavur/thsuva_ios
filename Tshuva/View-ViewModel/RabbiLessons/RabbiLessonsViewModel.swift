//
//  RabbiLessonsViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 26/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class RabbiLessonsViewModel{
    
    enum CursorType{
        case address, lessonType, desc, day, month, year, time
    }
    
    //MARK: - INPUT & OUTPUT
    let disposeBag                = DisposeBag()
    var lessons                   = BehaviorRelay<[Lesson]>(value: [])
    var lessonDidTap              = PublishSubject<IndexPath>()
    var noLessons                 = BehaviorRelay<Bool>(value: false)
    var isAddLessonMode           = BehaviorRelay<Bool>(value: false)
    
    var currentCursor:CursorType? = nil
    
    var addressSearch             = PublishSubject<String>()
    var addressResults            = BehaviorRelay<[Address]>(value: [])
    var addressSelected           = PublishSubject<IndexPath>()
    var addressViewHeight         = PublishSubject<CGFloat>()
    var addressSearchReturnDidTap = PublishSubject<Void>()
    var address                   = BehaviorRelay<Address?>(value: nil)
    
    var lessonTypeViewHeight      = PublishSubject<CGFloat>()
    var lessonType                = BehaviorRelay<LessonType?>(value: nil)
    var lessonTypeResults         = BehaviorRelay<[LessonType]>(value: AppData.shared.lessonTypes)
    var lessonTypeSelected        = PublishSubject<IndexPath>()
    var lessonTypeOpened          = BehaviorRelay<Bool>(value: false)
    
    var desc                      = BehaviorRelay<String>(value: "")
    var descCount                 = BehaviorRelay<String>(value: "0/140")
    var descDidBeginEditing       = PublishSubject<Void>()
    
    var days                      = BehaviorRelay<[String]>(value: hebrewCalendarDays())
    var day                       = BehaviorRelay<String?>(value: nil)
    var dayViewHeight             = PublishSubject<CGFloat>()
    var dayOpened                 = BehaviorRelay<Bool>(value: false)
    var dayDidSelect              = PublishSubject<IndexPath>()
    
    var months                    = BehaviorRelay<[String]>(value: hebrewCalendarMonth())
    var month                     = BehaviorRelay<String?>(value: nil)
    var monthViewHeight           = PublishSubject<CGFloat>()
    var monthOpened               = BehaviorRelay<Bool>(value: false)
    var monthDidSelect            = PublishSubject<IndexPath>()
    
    var years                     = BehaviorRelay<[String]>(value: hebrewCalendarYears())
    var year                      = BehaviorRelay<String?>(value: nil)
    var yearViewHeight            = PublishSubject<CGFloat>()
    var yearOpened                = BehaviorRelay<Bool>(value: false)
    var yearDidSelect             = PublishSubject<IndexPath>()
    
    var hoursAndMinutes           = BehaviorRelay<(hours: Int, minutes: Int)?>(value: nil)
    
    
    var createLesson              = PublishSubject<Void>()
    var editLesson                = PublishSubject<Lesson>()
    var lesson: Lesson?
    
    //MARK: - INIT
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(){
        
        isAddLessonMode.subscribe(onNext: { [weak self] (isAddMode) in
            
            // Changing mode to lesson's list have to clear all data
            if !isAddMode{
                self?.lesson = nil
                self?.address.accept(nil)
                self?.lessonType.accept(nil)
                self?.desc.accept("")
                self?.year.accept(nil)
                self?.month.accept(nil)
                self?.day.accept(nil)
                self?.hoursAndMinutes.accept(nil)
            }else{
                // Changing mode to add/edit lesson. If self.lesson != nil so the current mode is EDIT exists lesson
                if let l = self?.lesson{
                    self?.address.accept(l.address)
                    self?.lessonType.accept(l.lessonType)
                    self?.desc.accept(l.info)
                    self?.day.accept(l.date.formattedHebrewDayString)
                    self?.month.accept(l.date.formattedHebrewMonthString)
                    self?.year.accept(l.date.formattedHebrewYearString)
                    self?.hoursAndMinutes.accept((hours: l.date.hours, minutes: l.date.minutes))
                    
                }
            }
            
            }).disposed(by: disposeBag)
        
        editLesson.subscribe(onNext: { [weak self] (lesson) in
            
            self?.lesson = lesson
            self?.isAddLessonMode.accept(true)
            
            }).disposed(by: disposeBag)
        
        createLesson.subscribe(onNext: { [weak self] (_) in
            
            if self?.isDataValid() == false{
                SHOW_TOAST("נא למלא כל השדות!")
            }
            
            }).disposed(by: disposeBag)
        
        lessonObservers()
        
        descriptionObservers()

        addressObservers()

        yearObservers()

        monthObservers()
    
        dayObservers()

        lessonTypeObservers()
        
    }
    
    //MARK: - LESSONS
    private func lessonObservers(){
        
        AppData.shared.lessons.map { (lessons) in
            return lessons.filter({ $0.rabbiID == User.rabbi?.id }).sorted(by: { $0.date < $1.date })
        }.bind(to: lessons)
        .disposed(by: disposeBag)
        
        lessons
            .map({ $0.isEmpty })
            .bind(to: noLessons)
            .disposed(by: disposeBag)
        
        lessonDidTap
            .map({ [weak self] in
                self?.lessons.value[$0.row]
            })
            .subscribe(onNext: { (lesson) in
                guard let l = lesson else{ return }
                Coordinator.shared.pushLessonVC(with: l)
            })
            .disposed(by: disposeBag)
        
    }
    
    
    //MARK: - LESSON DESCRIPTION
    private func descriptionObservers(){
        
        descDidBeginEditing.subscribe(onNext: { [weak self] (_) in
            guard let `self` = self else{ return }
            self.currentCursor = .desc
            self.addressViewHeight.onNext(0)
            self.lessonTypeOpened.value ==> { self.lessonTypeOpened.accept(false) }
            self.dayOpened.value ==> { self.dayOpened.accept(false) }
            self.monthOpened.value ==> { self.monthOpened.accept(false) }
            self.yearOpened.value ==> { self.yearOpened.accept(false) }
            
            }).disposed(by: disposeBag)
        
        desc.subscribe(onNext: { [weak self] (text) in
            self?.descCount.accept("\(text.count)/140")
        }).disposed(by: disposeBag)
        
    }
    
    //MARK: - LESSON ADDRESS
    private func addressObservers(){
        
        addressSearch.filter({ [weak self] in
            guard let `self` = self else{ return false }
            self.lessonTypeOpened.value ==> { self.lessonTypeOpened.accept(false) }
            self.dayOpened.value ==> { self.dayOpened.accept(false) }
            self.monthOpened.value ==> { self.monthOpened.accept(false) }
            self.yearOpened.value ==> { self.yearOpened.accept(false) }
            return self.address.value?.fullAddress != $0
        })
            .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (text) in
                if self?.currentCursor != .address { return }
                self?.fetchAddresses(text: text)
            })
        .disposed(by: disposeBag)
        
        addressSearchReturnDidTap.subscribe(onNext: { [weak self] (_) in
            self?.address.accept(nil)
        })
        .disposed(by: disposeBag)
        
        addressSelected.map({ [unowned self] indexPath -> Address in
            self.addressViewHeight.onNext(0)
            return self.addressResults.value[indexPath.row]
            }).bind(to: address).disposed(by: disposeBag)
        
    }
    
    //MARK: - LESSON DATE
    private func yearObservers(){
        
        yearOpened.subscribe(onNext: { [weak self] (toOpen) in
            guard let `self` = self else{ return }
            
            if !toOpen{
                self.yearViewHeight.onNext(0)
            }else{
                
                self.addressViewHeight.onNext(0)
                self.dayOpened.value ==> { self.dayOpened.accept(false) }
                self.monthOpened.value ==> { self.monthOpened.accept(false) }
                self.lessonTypeOpened.value ==> { self.lessonTypeOpened.accept(false) }
                self.yearViewHeight.onNext(90)
            }
            
            }).disposed(by: disposeBag)
        
        yearDidSelect.map { [unowned self] (indexPath) -> String in
                self.yearOpened.accept(false)
                return self.years.value[indexPath.row]
        }
        .bind(to: self.year)
        .disposed(by: disposeBag)
        
    }
    
    private func monthObservers(){
        
        monthOpened.subscribe(onNext: { [weak self] (toOpen) in
            guard let `self` = self else{ return }
            
            if !toOpen{
                self.monthViewHeight.onNext(0)
            }else{
                
                self.addressViewHeight.onNext(0)
                self.dayOpened.value ==> { self.dayOpened.accept(false) }
                self.yearOpened.value ==> { self.yearOpened.accept(false) }
                self.lessonTypeOpened.value ==> { self.lessonTypeOpened.accept(false) }
                self.monthViewHeight.onNext(170)
            }
            
            }).disposed(by: disposeBag)
        
        monthDidSelect.map { [unowned self] (indexPath) -> String in
                self.monthOpened.accept(false)
                return self.months.value[indexPath.row]
        }
        .bind(to: self.month)
        .disposed(by: disposeBag)
        
    }
    
    private func dayObservers(){
        
        dayOpened.subscribe(onNext: { [weak self] (toOpen) in
            guard let `self` = self else{ return }
            
            if !toOpen{
                self.dayViewHeight.onNext(0)
            }else{
                
                self.addressViewHeight.onNext(0)
                self.monthOpened.value ==> { self.monthOpened.accept(false) }
                self.yearOpened.value ==> { self.yearOpened.accept(false) }
                self.lessonTypeOpened.value ==> { self.lessonTypeOpened.accept(false) }
                self.dayViewHeight.onNext(170)
            }
            
            }).disposed(by: disposeBag)
        
        dayDidSelect.map { [unowned self] (indexPath) -> String in
                self.dayOpened.accept(false)
                return self.days.value[indexPath.row]
        }
        .bind(to: self.day)
        .disposed(by: disposeBag)
        
    }
    
    //MARK: - LESSON'S TYPE
    private func lessonTypeObservers(){
        
        lessonTypeOpened.subscribe(onNext: { [weak self] (toOpen) in
            guard let `self` = self else{ return }
            
            if !toOpen{
                self.lessonTypeViewHeight.onNext(0)
            }else{
                
                self.addressViewHeight.onNext(0)
                self.dayOpened.value ==> { self.dayOpened.accept(false) }
                self.monthOpened.value ==> { self.monthOpened.accept(false) }
                self.yearOpened.value ==> { self.yearOpened.accept(false) }
                
                self.lessonType.accept(nil)
                self.lessonTypeViewHeight.onNext(210)
            }
            
            }).disposed(by: disposeBag)
        
        lessonTypeSelected
            .map { [unowned self] (indexPath) -> LessonType in
                self.lessonTypeOpened.accept(false)
                return self.lessonTypeResults.value[indexPath.row]
        }
        .bind(to: self.lessonType)
        .disposed(by: disposeBag)
        
    }
    
    //MARK: - CREATE & EDIT LESSON
    private func createLesson(lessonTypeID: Int, timestamp: Double, info: String, placeID: String, address: String, lat: Double, lon: Double){
        
        RequestManager.shared.createLesson(rabbiID: User.rabbi?.id ?? 0, lessonTypeID: lessonTypeID, date: timestamp, info: info, placeID: placeID, address: address, lat: lat, lon: lon).subscribe(onNext: { [weak self] (_) in
            self?.isAddLessonMode.accept(false)
        }, onError: { (error) in
            print(error.localizedDescription)
        }).disposed(by: disposeBag)
        
    }
    
    private func editLesson(id: Int, lessonTypeID: Int, timestamp: Double, info: String, placeID: String, address: String, lat: Double, lon: Double){
        
        RequestManager.shared.editLesson(id: id, rabbiID: User.rabbi?.id ?? 0, lessonTypeID: lessonTypeID, date: timestamp, info: info, placeID: placeID, address: address, lat: lat, lon: lon).subscribe(onNext: { [weak self] (_) in
            self?.isAddLessonMode.accept(false)
        }, onError: { (error) in
            print(error.localizedDescription)
        }).disposed(by: disposeBag)
        
    }
    
    //MARK: - HELPERS
    private func isDataValid() -> Bool{
        
        guard let lessonTypeID = lessonType.value?.id else{ return false }
        guard let date = isDateValid() else{ return false }
        guard let address = address.value else{ return false }
        
        if let location = AppData.shared.addresses.first(where: { $0.placeID == address.placeID })?.location {
           
            if let l = self.lesson{
                editLesson(id: l.id, lessonTypeID: lessonTypeID, timestamp: date.timeIntervalSince1970, info: desc.value, placeID: address.placeID, address: address.fullAddress, lat: location.coordinate.latitude, lon: location.coordinate.longitude)
            }else{
                createLesson(lessonTypeID: lessonTypeID, timestamp: date.timeIntervalSince1970, info: desc.value, placeID: address.placeID, address: address.fullAddress, lat: location.coordinate.latitude, lon: location.coordinate.longitude)
            }
            
        }else{
            
            RequestManager.shared.getLocationFor(placeID: address.placeID).subscribe(onNext: { [weak self] (loc) in
                
                if let l = self?.lesson{
                    self?.editLesson(id: l.id, lessonTypeID: lessonTypeID, timestamp: date.timeIntervalSince1970, info: self?.desc.value ?? "", placeID: address.placeID, address: address.fullAddress, lat: loc.lat, lon: loc.lon)
                }else{
                    self?.createLesson(lessonTypeID: lessonTypeID, timestamp: date.timeIntervalSince1970, info: self?.desc.value ?? "", placeID: address.placeID, address: address.fullAddress, lat: loc.lat, lon: loc.lon)
                }
                
            }, onError: { (error) in
                print(error.localizedDescription)
                }).disposed(by: disposeBag)
            
            
        }
        
        return true
        
    }
    
    private func isDateValid() -> Date? {
        
        guard let day = self.day.value else{ return nil }
        guard let month = self.month.value else{ return nil }
        guard let year = self.year.value else{ return nil }
        guard let hoursAndMinutes = self.hoursAndMinutes.value else{ return nil }
        
        let dateStr = "\(day) ב\(month) \(year)"
        let formatter = DateFormatter()
        formatter.locale = .init(identifier: "he_IL")
        let calendar = Calendar(identifier: .hebrew)
        formatter.calendar = calendar
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        
        guard let date = formatter.date(from: dateStr) else{ return nil }
        
        return calendar.date(bySettingHour: hoursAndMinutes.hours, minute: hoursAndMinutes.minutes, second: 0, of: date)
        
    }
    
    private func fetchAddresses(text: String){
        
        if text.isEmpty{
            self.addressViewHeight.onNext(0)
            return
        }
        
        RequestManager.shared.fetchAutocompleteAddresses(text: text)
        .subscribe(onNext: { [weak self] (addresses) in
            
            self?.addressResults.accept(addresses)
            
            if addresses.count == 0{
                self?.addressViewHeight.onNext(0)
            }else{
                self?.addressViewHeight.onNext(CGFloat(addresses.count) * 40 + 10)
            }
            
        }, onError: { [weak self] (error) in
            print(error.localizedDescription)
            self?.addressViewHeight.onNext(0)
        })
        .disposed(by: disposeBag)
    }
    
    private static func hebrewCalendarDays() -> [String] {
        return ["א", "ב", "ג", "ד", "ה", "ו", "ז", "ח", "ט", "י", "י״א", "י״ב", "י״ג", "י״ד", "ט״ו", "ט״ז", "י״ז", "י״ח", "י״ט", "כ", "כ״א", "כ״ב", "כ״ג", "כ״ד", "כ״ה", "כ״ו", "כ״ז", "כ״ח", "כ״ט", "ל"]
    }
    
    private static func hebrewCalendarMonth() -> [String] {
        return ["תשרי", "חשון", "כסלו", "טבת", "שבט", "אדר א׳", "אדר ב׳", "ניסן", "אייר", "סיון", "תמוז", "אב", "אלול"]
    }
    
    private static func hebrewCalendarYears() -> [String] {
        let yearsToAdd = 1
        var dateComponents = DateComponents()
        dateComponents.year = yearsToAdd
        let futureDate = Calendar.current.date(byAdding: dateComponents, to: Date())!
        
        return [Date().formattedHebrewYearString, futureDate.formattedHebrewYearString]
        
    }
    
    
}
