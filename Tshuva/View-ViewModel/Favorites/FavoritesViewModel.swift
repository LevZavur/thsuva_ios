//
//  FavoritesViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 21/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa



class FavoritesViewModel{
    
    let disposeBag      = DisposeBag()
    var favoriteRabbies = BehaviorRelay<[Rabbi]>(value: [])
    var rabbiDidTap     = PublishSubject<IndexPath>()
    var noFavorites     = BehaviorRelay<Bool>(value: false)
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(){
        
        AppData.shared.favoriteRabbiesID
            .map { ids -> [Rabbi] in
                AppData.shared.rabbies.filter({ ids.contains($0.id) })
        }
        .bind(to: favoriteRabbies)
        .disposed(by: disposeBag)
        
        rabbiDidTap.map({ [weak self] in
            self?.favoriteRabbies.value[$0.row] }).map({ (rabbi) -> [Lesson] in
            AppData.shared.lessons.value.filter({ $0.rabbiID == rabbi?.id })
        }).subscribe(onNext: { (lessons) in
            if lessons.count == 0 {
                SHOW_TOAST("לא נמצאו שיעורים לרב זה!")
                return
            }else if lessons.count == 1{
                Coordinator.shared.pushLessonVC(with: lessons[0])
            }else{
                Coordinator.shared.pushLessonsVC(with: lessons)
            }
            
            
        })
        .disposed(by: disposeBag)
        
        favoriteRabbies.map({ $0.count == 0 }).bind(to: noFavorites).disposed(by: disposeBag)
        
    }
    
}
