//
//  FavoritesVC.swift
//  Tshuva
//
//  Created by Denis W. on 13/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAnimated



class FavoritesVC: BaseVC {
    
    @IBOutlet weak var tblFavorites: UITableView!
    @IBOutlet weak var viewNoFavorites: UIView!
    
    
    var viewModel = FavoritesViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.noFavorites.map({ CGFloat($0 == true ? 1 : 0) }).bind(to: viewNoFavorites.rx.animated.fade(duration: 0.5).alpha).disposed(by: disposeBag)
        
        viewModel.favoriteRabbies
            .bind(to: tblFavorites.rx.items(cellIdentifier: "FavoritesCell", cellType: FavoritesCell.self)){ row, rabbi, cell in
                cell.viewModel = FavoritesCellViewModel(rabbi: rabbi)
        }.disposed(by: disposeBag)
        
        tblFavorites.rx.itemSelected.bind(to: viewModel.rabbiDidTap).disposed(by: disposeBag)
        
        
        
    }


}
