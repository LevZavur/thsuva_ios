//
//  LessonVC.swift
//  Tshuva
//
//  Created by Denis W. on 19/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import CoreLocation



class LessonVC: BaseVC {
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnNavigate: UIButton!
    @IBOutlet weak var lblRabbiName: UILabel!
    @IBOutlet weak var lblLessonSubject: UILabel!
    @IBOutlet weak var lblLessonDesc: UILabel!
    @IBOutlet weak var lblLessonData: UILabel!
    
    
    var viewModel: LessonViewModel!

    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnFavorite.rx.tap.bind(to: viewModel.favoriteDidTap).disposed(by: disposeBag)
        
        btnNavigate.rx.tap.bind(to: viewModel.navigateDidTap).disposed(by: disposeBag)
        
        viewModel.favoriteImage.filter({ [weak self] in
            $0.pngData() != self?.btnFavorite.imageView?.image?.pngData()
        }).bind(to: btnFavorite.rx.animated.flip(.right, duration: 0.5).image).disposed(by: disposeBag)
        
        viewModel.openNavigationOptions.subscribe(onNext: { [weak self] (params) in
            self?.presentNavigateOptions(location: params.location, locationName: params.address)
        }, onError: { (error) in
            SHOW_TOAST(error.localizedDescription)
        })
            .disposed(by: disposeBag)
        
        viewModel.rabbiName.bind(to: lblRabbiName.rx.text).disposed(by: disposeBag)
        viewModel.lessonSubject.bind(to: lblLessonSubject.rx.text).disposed(by: disposeBag)
        viewModel.lessonDescription.bind(to: lblLessonDesc.rx.text).disposed(by: disposeBag)
        viewModel.lessonsData.bind(to: lblLessonData.rx.text).disposed(by: disposeBag)
        
        viewModel.configurePublishers()
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewShadow.dropShadow(onlyBottom: true, color: .black)
    }


}
