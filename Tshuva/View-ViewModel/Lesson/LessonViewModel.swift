//
//  LessonViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 20/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation


class LessonViewModel {
    
    
    var lesson                : Lesson
    let disposeBag            = DisposeBag()
    var favoriteImage         = PublishSubject<UIImage>()
    var favoriteDidTap        = PublishSubject<Void>()
    var openNavigationOptions = PublishSubject<(location: CLLocation, address: String)>()
    var navigateDidTap        = PublishSubject<Void>()
    var rabbiName             = PublishSubject<String>()
    var lessonSubject         = PublishSubject<String>()
    var lessonDescription     = PublishSubject<String>()
    var lessonsData           = PublishSubject<String>()
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(lesson: Lesson){
        
        self.lesson = lesson
        
        favoriteDidTap.subscribe(onNext: { (_) in
            AppData.shared.updateFavorite(with: lesson.rabbiID)
        })
        .disposed(by: disposeBag)
        
        navigateDidTap.subscribe(onNext: { [weak self] (_) in
            guard let location = lesson.address?.location else{
                self?.openNavigationOptions.onError(TshuvaError.locationFailed("Lesson's location error"))
                return
            }
            self?.openNavigationOptions.onNext((location: location, address: lesson.address?.fullAddress ?? ""))
        })
        .disposed(by: disposeBag)
        
        
    }
    
    func configurePublishers() {
        
        AppData.shared.favoriteRabbiesID.map({ [unowned self] in
            $0.contains(self.lesson.rabbiID) ? UIImage(named: "inside info_blue_full")! : UIImage(named: "inside info_blue_big_heart")!
        }).bind(to: favoriteImage).disposed(by: disposeBag)
        
        rabbiName.onNext(lesson.rabbi?.name ?? "")
        
        lessonSubject.onNext("נושא השיעור: \(lesson.lessonType?.name ?? "")")
        
        lessonDescription.onNext(lesson.info)
        lessonsData.onNext("\(lesson.date.formattedHebrewWeekDayString) \(lesson.date.formattedHebrewString)\n\n\(lesson.date.formattedTimeString)\n\n\(lesson.address?.fullAddress ?? "")\n\n\(lesson.rabbi?.phone ?? "")")
        
    }
    
    
}
