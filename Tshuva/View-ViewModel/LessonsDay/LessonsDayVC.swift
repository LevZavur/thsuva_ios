//
//  LessonsDayVC.swift
//  Tshuva
//
//  Created by Denis W. on 20/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit


class LessonsDayVC: BaseVC {
    
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var textVBody: UITextView!
    
    
    var viewModel = LessonsDayViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.image.bind(to: iv.rx.image).disposed(by: disposeBag)
        viewModel.title.bind(to: lblTitle.rx.text).disposed(by: disposeBag)
        viewModel.body.bind(to: textVBody.rx.text).disposed(by: disposeBag)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        viewShadow.dropShadow(onlyBottom: true, color: .black)
    }


}
