//
//  LessonsDayViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 20/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import SDWebImage


class LessonsDayViewModel{
    
    let disposeBag = DisposeBag()
    var image      = BehaviorRelay<UIImage?>(value: nil)
    var title      = BehaviorRelay<String?>(value: nil)
    var body       = BehaviorRelay<String?>(value: nil)
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(){
        
        SDWebImageManager.shared().loadImage(with: AppData.shared.lessonsDay?.imageURL, options: .highPriority, progress: nil) { [weak self] (image, _, _, _, _, _) in
            self?.image.accept(image)
        }
        
        title.accept(AppData.shared.lessonsDay?.title)
        body.accept(AppData.shared.lessonsDay?.body)
        
    }

}
