//
//  LessonsVC.swift
//  Tshuva
//
//  Created by Denis W. on 19/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



class LessonsVC: BaseVC {

    @IBOutlet weak var tblLessonsList: UITableView!
    
    var viewModel: LessonsViewModel!
    
    //MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblLessonsList.rx.itemSelected.bind(to: viewModel.lessonSelected).disposed(by: disposeBag)
        
        viewModel.lessons
            .bind(to: tblLessonsList.rx.items(cellIdentifier: "LessonCell", cellType: LessonCell.self) ){ row, lesson, cell in
                cell.viewModel = LessonCellViewModel(lesson: lesson)
        }
        .disposed(by: disposeBag)

//        viewModel.lessons.bind(to: tblLessonsList.rx.items){ tableView, row, lesson in
//            let cell = tableView.dequeueReusableCell(withIdentifier: "LessonCell", for: IndexPath(row: row, section: 0)) as! LessonCell
//            if cell.viewModel == nil{
//                let viewModel = LessonCellViewModel(lesson: lesson)
//                cell.viewModel = viewModel
//            }
//            return cell
//        }
//        .disposed(by: disposeBag)
        
        viewModel.navigateToLesson.subscribe(onNext: { (lesson) in
            Coordinator.shared.pushLessonVC(with: lesson)
        })
        .disposed(by: disposeBag)
        
    }

}
