//
//  LessonsViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 19/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class LessonsViewModel {
    
    let disposeBag       = DisposeBag()
    var lessons          : BehaviorRelay<[Lesson]>
    var lessonSelected   = PublishSubject<IndexPath>()
    var navigateToLesson = PublishSubject<Lesson>()
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(lessons: [Lesson]){
        
        self.lessons = BehaviorRelay(value: lessons)
        
        lessonSelected.map({ [unowned self] in
            self.lessons.value[$0.row]
        }).bind(to: navigateToLesson).disposed(by: disposeBag)
        
    }
    
}
