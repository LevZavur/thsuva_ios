//
//  TermsVC.swift
//  Tshuva
//
//  Created by Denis W. on 14/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxAnimated



class TermsVC: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var btnEnter: UIButton!
    @IBOutlet weak var textViewTerms: UITextView!
    @IBOutlet weak var lblSubtitle: UILabel!{
        didSet{
            let attrString = NSMutableAttributedString()
            attrString
                .normal("עם כניסתי, הנני מאשר את ", fontSize: 20)
                .underlined("התקנון", fontSize: 20, color: Colors.lightBlue)
            
            self.lblSubtitle.attributedText = attrString
        }
    }
    
    let disposeBag = DisposeBag()
    let viewModel = TermsViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer()
        self.lblSubtitle.addGestureRecognizer(tap)
        self.lblSubtitle.isUserInteractionEnabled = true
        
        
        tap.rx.event
        .map({ $0.didTap(text: "התקנון", inLabel: self.lblSubtitle) })
        .filter({ $0 == true })
        .bind(to: viewModel.termsShown)
        .disposed(by: disposeBag)
        
        btnBack.rx.tap
            .bind(to: viewModel.backTapped)
            .disposed(by: disposeBag)
        
        btnEnter.rx.tap
            .bind(to: viewModel.enterTapped)
            .disposed(by: disposeBag)
        
        viewModel.termsShown.map({ $0 == true ? 1 : 0 }).bind(animated: textViewTerms.rx.animated.fade(duration: 0.5).alpha).disposed(by: disposeBag)
        
        viewModel.isBackShown.map({ $0 == true ? 1 : 0 }).bind(animated: btnBack.rx.animated.fade(duration: 0.5).alpha).disposed(by: disposeBag)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        viewShadow.dropShadow(onlyBottom: true, color: .black)
    }

}
