//
//  TermsViewModel.swift
//  Tshuva
//
//  Created by Denis W. on 14/11/2019.
//  Copyright © 2019 Bigapps Interactive. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class TermsViewModel{
    
    let disposeBag  = DisposeBag()
    var isBackShown = BehaviorRelay<Bool>(value: false)
    var termsShown  = PublishSubject<Bool>()
    var backTapped  = PublishSubject<Void>()
    var enterTapped = PublishSubject<Void>()
    
    deinit {
        print("-------DEINIT---------")
        print(self)
        print("-------DEINIT---------")
    }
    
    init(){
        
        termsShown
            .bind(to: isBackShown)
            .disposed(by: disposeBag)
        
        backTapped
            .subscribe(onNext: { [weak self] (_) in
                self?.isBackShown.accept(false)
                self?.termsShown.onNext(false)
            })
            .disposed(by: disposeBag)
        
        enterTapped
            .subscribe(onNext: { (_) in
                User.termsShown()
                Coordinator.shared.pushMainVC()
            })
            .disposed(by: disposeBag)
    }
}
